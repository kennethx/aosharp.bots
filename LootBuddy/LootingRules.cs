﻿using System;
using System.Collections.Generic;
using System.IO;
using AOSharp.Core;
using Newtonsoft.Json;
using AOSharp.Core.UI;
using AOSharp.Core.Inventory;

namespace LootBuddy
{
    public class LootingRules
    {
        private static LootingRulesConfig _rulesConfig = new LootingRulesConfig();

        private static LootingRules lootingrules = new LootingRules();

        public static string Location = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy\\LootingRules.json";

        protected string _path;

        public static LootingRules Load(string path)
        {
            try
            {
                _rulesConfig = JsonConvert.DeserializeObject<LootingRulesConfig>(File.ReadAllText(path));

                lootingrules._path = path;

            }
            catch
            {
            }

            return lootingrules;
        }

        public static void Save()
        {
            try
            {
                if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                    Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy"))
                    Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootBuddy");

                File.WriteAllText(Location, JsonConvert.SerializeObject(_rulesConfig, Formatting.Indented));
            }
            catch (Exception e)
            {
                Chat.WriteLine(e);
            }
        }

        public static void Add(int minQl, int maxQl, string name)
        {
            ItemRule newRule = new ItemRule(minQl, maxQl, name);
            if (!_rulesConfig.Rules.Contains(newRule))
            {
                _rulesConfig.Rules.Add(newRule);
            }
        }

        public static void Remove(int index)
        {
            _rulesConfig.Rules.RemoveAt(index);
        }

        public static void Clear()
        {
            _rulesConfig.Rules.Clear();
        }

        public static bool Apply(Item item)
        {
            return _rulesConfig.Rules.Find(rules => rules.Apply(item)) != null;
        }

        internal static void DumpToChat()
        {
            if (_rulesConfig.Rules.Count == 0)
                Chat.WriteLine($"List empty.");

            int index = 0;

            foreach (ItemRule rule in _rulesConfig.Rules)
            {
                Chat.WriteLine($"");
                Chat.WriteLine($"Rule #{index};");
                Chat.WriteLine($"Min Ql - {rule.minQl}");
                Chat.WriteLine($"Max Ql - {rule.maxQl}");
                Chat.WriteLine($"Item - {rule.name}");
                index++;
            }
        }
    }

    class LootingRulesConfig
    {
        public readonly List<ItemRule> Rules = new List<ItemRule>();
    }

    public class ItemRule
    {
        public int minQl;
        public int maxQl;
        public string name;

        public ItemRule(int minQl, int maxQl, string name)
        {
            this.minQl = minQl;
            this.maxQl = maxQl;
            this.name = name;
        }

        public bool Apply(Item item)
        {
            return item.Name.Contains(name) && item.QualityLevel >= minQl && item.QualityLevel <= maxQl;
        }
    }
}
