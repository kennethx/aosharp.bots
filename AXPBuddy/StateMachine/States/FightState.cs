﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Configuration;
using System.Linq;

namespace AXPBuddy
{
    public class FightState : IState
    {
        public const double FightTimeout = 60f;
        private double _fightStartTime;
        private int _aggToolCounter = 0;

        public IState GetNextState()
        {
            if (AXPBuddy._attackTarget?.IsAlive == false)
            {
                AXPBuddy._attackTarget = null;

                if (AXPBuddy._settings["Toggle"].AsBool())
                    return new PatrolState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                AXPBuddy._attackTarget = null;

                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    return new DiedState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            _aggToolCounter = 0;

            //Chat.WriteLine($"FightState::OnStateEnter");
        }

        public void OnStateExit()
        {
            _aggToolCounter = 0;

            //Chat.WriteLine("FightState::OnStateExit");
        }

        public void Tick()
        {
            if (AXPBuddy._attackTarget == null) { return; }

            if (AXPBuddy._settings["AggroTool"].AsBool())
            {
                if (!AXPBuddy._attackTarget.IsInLineOfSight && !AXPBuddy.NavMeshMovementController.IsNavigating)
                {
                    AXPBuddy.NavMeshMovementController.SetNavMeshDestination(AXPBuddy._attackTarget.Position);
                }

                if (AXPBuddy._attackTarget.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 14f)
                {
                    if (DynelManager.LocalPlayer.FightingTarget == null
                        && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                    {
                        AXPBuddy.NavMeshMovementController.Halt();
                        DynelManager.LocalPlayer.Attack(AXPBuddy._attackTarget);
                        Chat.WriteLine($"Attacking {AXPBuddy._attackTarget.Name}.");
                        _fightStartTime = Time.NormalTime;
                    }
                }
                else
                {
                    if (_aggToolCounter >= 3)
                    {
                        if (!AXPBuddy._attackTarget.IsMoving && AXPBuddy._attackTarget.IsInLineOfSight)
                        {
                            AXPBuddy.NavMeshMovementController.SetNavMeshDestination(AXPBuddy._attackTarget.Position);
                        }
                    }
                    else if (Inventory.Find(83920, 83919, out Item aggroTool))
                    {
                        if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                        {
                            AXPBuddy.NavMeshMovementController.Halt();
                            aggroTool.Use(AXPBuddy._attackTarget, true);
                            _aggToolCounter++;
                            return;
                        }
                    }
                    else if (Inventory.Find(244655, 244655, out Item scorpioTool))
                    {
                        if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                        {
                            AXPBuddy.NavMeshMovementController.Halt();
                            scorpioTool.Use(AXPBuddy._attackTarget, true);
                            _aggToolCounter++;
                            return;
                        }
                    }
                    else if (Inventory.Find(83919, 83919, out Item aggroMultiTool)) // To-do could be wrong
                    {
                        if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                        {
                            AXPBuddy.NavMeshMovementController.Halt();
                            aggroMultiTool.Use(AXPBuddy._attackTarget, true);
                            _aggToolCounter++;
                            return;
                        }
                    }
                    else if (Inventory.Find(253186, 253186, out Item EmertoLow))
                    {
                        if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                        {
                            AXPBuddy.NavMeshMovementController.Halt();
                            EmertoLow.Use(AXPBuddy._attackTarget, true);
                            _aggToolCounter++;
                            return;
                        }
                    }
                    else if (Inventory.Find(253187, 253187, out Item EmertoHigh))
                    {
                        if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                        {
                            AXPBuddy.NavMeshMovementController.Halt();
                            EmertoHigh.Use(AXPBuddy._attackTarget, true);
                            _aggToolCounter++;
                            return;
                        }
                    }
                }
            }
            else
            {
                if (DynelManager.LocalPlayer.FightingTarget == null
                    && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                {
                    if (AXPBuddy._attackTarget.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 6f
                        && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                        || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                    {
                        AXPBuddy.NavMeshMovementController.Halt();
                        DynelManager.LocalPlayer.Attack(AXPBuddy._attackTarget);
                        Chat.WriteLine($"Attacking {AXPBuddy._attackTarget.Name}.");
                        _fightStartTime = Time.NormalTime;
                    }
                    else if (AXPBuddy._attackTarget.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                        && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
                    {
                        AXPBuddy.NavMeshMovementController.Halt();
                        DynelManager.LocalPlayer.Attack(AXPBuddy._attackTarget);
                        Chat.WriteLine($"Attacking {AXPBuddy._attackTarget.Name}.");
                        _fightStartTime = Time.NormalTime;
                    }
                }

                if (!AXPBuddy._attackTarget.IsMoving
                    && AXPBuddy._attackTarget.FightingTarget != null)
                {
                    if (!AXPBuddy._attackTarget.IsInLineOfSight && !AXPBuddy.NavMeshMovementController.IsNavigating)
                    {
                        AXPBuddy.NavMeshMovementController.SetNavMeshDestination(AXPBuddy._attackTarget.Position);
                    }

                    if (AXPBuddy._attackTarget.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 6f
                        && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                        || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                    {
                        AXPBuddy.NavMeshMovementController.SetNavMeshDestination(AXPBuddy._attackTarget.Position);
                    }
                    else if (AXPBuddy._attackTarget.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                        && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
                    {
                        AXPBuddy.NavMeshMovementController.SetNavMeshDestination(AXPBuddy._attackTarget.Position);
                    }
                }
            }
        }

        public static CharacterWieldedWeapon GetWieldedWeapons(SimpleChar local) => (CharacterWieldedWeapon)local.GetStat(Stat.EquippedWeapons);
    }

    [Flags]
    public enum CharacterWieldedWeapon
    {
        Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
        MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
        Melee = 0x02,             // 0x00000000000000000010b
        Ranged = 0x04,            // 0x00000000000000000100b
        Bow = 0x08,               // 0x00000000000000001000b
        Smg = 0x10,               // 0x00000000000000010000b
        Edged1H = 0x20,           // 0x00000000000000100000b
        Blunt1H = 0x40,           // 0x00000000000001000000b
        Edged2H = 0x80,           // 0x00000000000010000000b
        Blunt2H = 0x100,          // 0x00000000000100000000b
        Piercing = 0x200,         // 0x00000000001000000000b
        Pistol = 0x400,           // 0x00000000010000000000b
        AssaultRifle = 0x800,     // 0x00000000100000000000b
        Rifle = 0x1000,           // 0x00000001000000000000b
        Shotgun = 0x2000,         // 0x00000010000000000000b
        Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
        MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
        RangedEnergy = 0x10000,   // 0x00010000000000000000b
        Grenade2 = 0x20000,        // 0x00100000000000000000b
        HeavyWeapons = 0x40000,   // 0x01000000000000000000b
    }
}
