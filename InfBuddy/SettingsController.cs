﻿using AOSharp.Common.GameData;
using AOSharp.Core.UI;
using AOSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData.UI;

namespace InfBuddy
{
    public static class SettingsController
    {
        private static List<Settings> settingsToSave = new List<Settings>();
        public static Dictionary<string, string> settingsWindows = new Dictionary<string, string>();
        private static bool IsCommandRegistered;

        public static Window settingsWindow;
        public static View settingsView;

        public static string InfBuddyFaction = String.Empty;
        public static string InfBuddyDifficulty = String.Empty;
        public static string InfBuddyChannel = String.Empty;


        public static void RegisterCharacters(Settings settings)
        {
            RegisterChatCommandIfNotRegistered();
            settingsToSave.Add(settings);
        }

        public static void RegisterSettingsWindow(string settingsName, string settingsWindowPath, Settings settings)
        {
            RegisterChatCommandIfNotRegistered();
            settingsWindows[settingsName] = settingsWindowPath;
            settingsToSave.Add(settings);
        }

        public static void RegisterSettings(Settings settings)
        {
            RegisterChatCommandIfNotRegistered();
            settingsToSave.Add(settings);
        }

        public static void CleanUp()
        {
            settingsToSave.ForEach(settings => settings.Save());
        }

        private static void RegisterChatCommandIfNotRegistered()
        {
            if (!IsCommandRegistered)
            {
                Chat.RegisterCommand("infbuddy", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    try
                    {
                        settingsWindow = Window.Create(new Rect(50, 50, 300, 300), "InfBuddy", "Settings", WindowStyle.Default, WindowFlags.AutoScale);

                        if (settingsWindow != null && !settingsWindow.IsVisible)
                        {
                            AppendSettingsTab("InfBuddy", settingsWindow);

                            if (InfBuddyFaction != String.Empty)
                            {
                                settingsWindow.FindView("FactionBox", out TextInputView textinput);
                                if (textinput != null)
                                    textinput.Text = InfBuddyFaction;
                            }

                            if (InfBuddyDifficulty != String.Empty)
                            {
                                settingsWindow.FindView("DifficultyBox", out TextInputView textinput);
                                if (textinput != null)
                                    textinput.Text = InfBuddyDifficulty;
                            }

                            if (InfBuddyChannel != String.Empty)
                            {
                                settingsWindow.FindView("ChannelBox", out TextInputView textinput);
                                if (textinput != null)
                                    textinput.Text = InfBuddyChannel;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Chat.WriteLine(e);
                    }

                    //try
                    //{
                    //    settingsWindow = Window.Create(new Rect(50, 50, 300, 300), "InfBuddy", "Settings", WindowStyle.Default, WindowFlags.AutoScale);

                    //    foreach (string settingsName in settingsWindows.Keys)
                    //    {
                    //        AppendSettingsTab(settingsName, settingsWindow);

                    //        if (InfBuddyFaction != String.Empty)
                    //        {
                    //            settingsWindow.FindView("FactionBox", out TextInputView textinput);
                    //            if (textinput != null)
                    //                textinput.Text = InfBuddyFaction;
                    //        }

                    //        if (InfBuddyDifficulty != String.Empty)
                    //        {
                    //            settingsWindow.FindView("DifficultyBox", out TextInputView textinput);
                    //            if (textinput != null)
                    //                textinput.Text = InfBuddyDifficulty;
                    //        }

                    //        if (InfBuddyChannel != String.Empty)
                    //        {
                    //            settingsWindow.FindView("ChannelBox", out TextInputView textinput);
                    //            if (textinput != null)
                    //                textinput.Text = InfBuddyChannel;
                    //        }
                    //    }
                    //}
                    //catch (Exception e)
                    //{
                    //    Chat.WriteLine(e);
                    //}
                });

                IsCommandRegistered = true;
            }
        }

        public static void AppendSettingsTab(String settingsName, Window testWindow)
        {
            String settingsWindowXmlPath = settingsWindows[settingsName];
            settingsView = View.CreateFromXml(settingsWindowXmlPath);
            if (settingsView != null)
            {
                testWindow.AppendTab(settingsName, settingsView);
                testWindow.Show(true);
            }
            else
            {
                Chat.WriteLine("Failed to load settings schema from " + settingsWindowXmlPath);
            }
        }
    }
}
