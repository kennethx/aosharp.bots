﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AttackBuddy
{
    public class FightState : PositionHolder, IState
    {
        public const double FightTimeout = 45f;
        
        private double _fightStartTime;
        public static float _tetherDistance;

        public static int _aggToolCounter = 0;

        public static List<Identity> corpseToLootIdentity = new List<Identity>();
        public static List<Corpse> corpsesToLoot = new List<Corpse>();
        public static List<Identity> lootedCorpses = new List<Identity>();

        public static List<int> _ignoreTargetIdentity = new List<int>();

        private SimpleChar _target;

        public FightState(SimpleChar target) : base(Constants._posToDefend, 3f, 1)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (_target?.IsValid == false ||
               _target?.IsAlive == false ||
               _target?.IsInLineOfSight == false ||
               //_target?.DistanceFrom(DynelManager.LocalPlayer) > SettingsController.AttackBuddyThreatRange ||
               _target.IsPet ||
               (_aggToolCounter >= 5 && _ignoreTargetIdentity.Count >= 1) ||
               (Time.NormalTime > _fightStartTime + FightTimeout && _target?.MaxHealth <= 999999))
            {
                _target = null;

                if (AttackBuddy.AttackBuddySettings["Attack"].AsBool())
                    return new ScanState();

                if (AttackBuddy.AttackBuddySettings["Defend"].AsBool())
                {
                    if (AttackBuddy.AttackBuddySettings["LootDelay"].AsBool())
                    {
                        List<SimpleChar> mobs = DynelManager.NPCs
                            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                                && !AttackBuddy.NamesToIgnore.Contains(c.Name) && c.IsAlive && c.IsInLineOfSight)
                            .ToList();

                            //.Where(c => !AttackBuddy.NamesToIgnore.Contains(c.Name))
                            //.Where(c => c.IsAlive && c.IsInLineOfSight)
                            //.ToList();

                        if (mobs?.Count == 0)
                        {
                            return new LootState();
                        }
                        else
                        {
                            return new DefendState();
                        }
                    }
                    else
                        return new DefendState();
                }

                if (AttackBuddy.AttackBuddySettings["Roam"].AsBool())
                {
                    if (AttackBuddy.AttackBuddySettings["LootDelay"].AsBool())
                    {
                        List<SimpleChar> mobs = DynelManager.NPCs
                            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                                && !AttackBuddy.NamesToIgnore.Contains(c.Name) && c.IsAlive && c.IsInLineOfSight)
                            .ToList();

                            //.Where(c => !AttackBuddy.NamesToIgnore.Contains(c.Name))
                            //.Where(c => c.IsAlive && c.IsInLineOfSight)
                            //.ToList();

                        if (mobs?.Count == 0)
                        {
                            return new LootState();
                        }
                        else
                        {
                            return new RoamState();
                        }
                    }
                    else
                        return new RoamState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");

            _fightStartTime = Time.NormalTime;

            _aggToolCounter = 0;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            _aggToolCounter = 0;

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }

        //private bool IsFightingAny(SimpleChar target)
        //{
        //    if (Team.IsInTeam)
        //    {
        //        if (target.FightingTarget == null) { return true; }

        //        if (target.FightingTarget != null && target.FightingTarget == DynelManager.LocalPlayer) { return true; }

        //        if (target.FightingTarget != null && (Team.Members.Where(c => c.Name == target.FightingTarget.Name).Any()
        //            || AttackBuddy.OSHelpers.Where(c => target.FightingTarget.Name == c).Any())) { return true; }

        //        if (target.FightingTarget != null && target.FightingTarget.IsPet
        //            && Team.Members.Where(c => c.Name == target.FightingTarget.Name).Any()) { return true; }

        //        if (target.FightingTarget != null && (target.FightingTarget.Name == "Guardian Spirit of Purification"
        //            || target.FightingTarget.Name == "Rookie Alien Hunter")) { return true; }

        //        return false;
        //    }
        //    else
        //    {
        //        if (target.FightingTarget == null) { return true; }

        //        if (target.FightingTarget != null && target.FightingTarget == DynelManager.LocalPlayer) { return true; }

        //        if (target.FightingTarget != null && (target.FightingTarget.Name == DynelManager.LocalPlayer.Name
        //            || AttackBuddy.OSHelpers.Where(c => target.FightingTarget.Name == c).Any())) { return true; }

        //        if (target.FightingTarget != null &&
        //            DynelManager.LocalPlayer.Pets.Where(c => target.FightingTarget.Name == c.Character.Name).Any()) { return true; }

        //        if (target.FightingTarget != null && (target.FightingTarget.Name == "Guardian Spirit of Purification"
        //            || target.FightingTarget.Name == "Rookie Alien Hunter")) { return true; }

        //        return false;
        //    }
        //}

        //public static bool BossHasCorrespondingBuff(int buff)
        //{
        //    SimpleChar boss = DynelManager.NPCs
        //                        .Where(c => c.Name == "Kyr'Ozch Technician" && c.Buffs.Contains(buff))
        //                        .FirstOrDefault();

        //                        //.Where(c => c.Buffs.Contains(buff))
        //                        //.FirstOrDefault();

        //    if (boss != null)
        //        return true;
        //    else
        //        return false;
        //}

        public static CharacterWieldedWeapon GetWieldedWeapons(SimpleChar local) => (CharacterWieldedWeapon)local.GetStat(Stat.EquippedWeapons);

        public void Tick()
        {
            if (_target == null)
                return;

            if (AttackBuddy.GetLeader() != null)
            {
                if (DynelManager.LocalPlayer.FightingTarget != null && DynelManager.LocalPlayer.FightingTarget.IsPlayer)
                {
                    DynelManager.LocalPlayer.Attack(_target);
                    return;
                }

                if (DynelManager.LocalPlayer.FightingTarget != null && DynelManager.LocalPlayer.FightingTarget.MaxHealth >= 1000000 &&
                    (DynelManager.LocalPlayer.FightingTarget.Buffs.Contains(253953) || DynelManager.LocalPlayer.FightingTarget.Buffs.Contains(205607)))
                {
                    DynelManager.LocalPlayer.StopAttack();
                    return;
                }

                //if (Playfield.ModelIdentity.Instance == 6123)
                //{
                //    _switchMob = DynelManager.NPCs
                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange)
                //        .Where(c => !AttackBuddy.NamesToIgnore.Contains(c.Name))

                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                //            && !AttackBuddy.NamesToIgnore.Contains(c.Name)
                //            && c.Name != "Alien Cocoon" && c.Name != "Zix"
                //            && c.IsAlive && c.IsInLineOfSight && c.Name != "Kyr'Ozch Maid"
                //            && c.Name != "Kyr'Ozch Technician" && c.Name != "Defense Drone Tower"
                //            && c.Name != "Control Leech"
                //            && c.Name != "Alien Precision Tower" && c.Name != "Alien Charging Tower"
                //            && c.Name != "Alien Shield Tower" && c.Name != "Kyr'Ozch Technician"
                //            && c.MaxHealth < 1000000 && IsFightingAny(c))
                //        .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                //        .OrderBy(c => c.HealthPercent)
                //        .OrderByDescending(c => c.Name == "Kyr'Ozch Nurse")
                //        .OrderByDescending(c => c.Name == "Kyr'Ozch Offspring")
                //        .OrderByDescending(c => c.Name == "Rimah Corsuezo")
                //        .FirstOrDefault();

                //    _switchMobPrecision = DynelManager.NPCs
                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                //            && BossHasCorrespondingBuff(287525) && c.Name == "Alien Precision Tower")
                //        .FirstOrDefault();

                //    .Where(c => BossHasCorrespondingBuff(287525))
                //    .Where(c => c.Name == "Alien Precision Tower")
                //    .FirstOrDefault();

                //    _switchMobCharging = DynelManager.NPCs
                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                //            && BossHasCorrespondingBuff(287515) && c.Name == "Alien Charging Tower")
                //        .FirstOrDefault();

                //    .Where(c => BossHasCorrespondingBuff(287515))
                //    .Where(c => c.Name == "Alien Charging Tower")
                //    .FirstOrDefault();

                //    _switchMobShield = DynelManager.NPCs
                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                //            && BossHasCorrespondingBuff(287526) && c.Name == "Alien Shield Tower")
                //        .FirstOrDefault();

                //    .Where(c => BossHasCorrespondingBuff(287526))
                //    .Where(c => c.Name == "Alien Shield Tower")
                //    .FirstOrDefault();

                //}
                //else
                //{
                //    _switchMob = DynelManager.NPCs
                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange)
                //        .Where(c => !AttackBuddy.NamesToIgnore.Contains(c.Name))

                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                //            && !AttackBuddy.NamesToIgnore.Contains(c.Name)
                //            && c.Name != "Zix" && !c.Name.Contains("sapling")
                //            && c.IsAlive && c.IsInLineOfSight && c.MaxHealth < 1000000
                //            && IsFightingAny(c) && (c.Name == "Devoted Fanatic" || c.Name == "Hallowed Acolyte" || c.Name == "Hand of the Colonel"
                //         || c.Name == "Hacker'Uri" || c.Name == "The Sacrifice" || c.Name == "Corrupted Xan-Len"
                //          || c.Name == "Blue Tower" || c.Name == "Green Tower" || c.Name == "Drone Harvester - Jaax'Sinuh"
                //           || c.Name == "Support Sentry - Ilari'Uri" || c.Name == "Fanatic" || c.Name == "Alien Cocoon"))
                //        .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                //        .OrderBy(c => c.HealthPercent)
                //        .OrderByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
                //        .OrderByDescending(c => c.Name == "Alien Cocoon")
                //        .OrderByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
                //        .FirstOrDefault();

                //    _mob = DynelManager.NPCs
                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange)
                //        .Where(c => !AttackBuddy.NamesToIgnore.Contains(c.Name))

                //        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                //            && !AttackBuddy.NamesToIgnore.Contains(c.Name)
                //            && c.Name != "Zix" && !c.Name.Contains("sapling") && c.IsAlive
                //            && c.IsInLineOfSight && c.MaxHealth < 1000000 && IsFightingAny(c)
                //            && c.Name != "Devoted Fanatic" && c.Name != "Hallowed Acolyte" && c.Name != "Hand of the Colonel"
                //            && c.Name != "Hacker'Uri" && c.Name != "The Sacrifice" && c.Name != "Corrupted Xan-Len"
                //            && c.Name != "Blue Tower" && c.Name != "Green Tower" && c.Name != "Drone Harvester - Jaax'Sinuh"
                //            && c.Name != "Support Sentry - Ilari'Uri" && c.Name != "Fanatic" && c.Name != "Alien Cocoon"
                //            && c.Name != "Kyr'Ozch Technician")
                //        .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                //        .OrderBy(c => c.HealthPercent)
                //        .FirstOrDefault();
                //}

                if (DynelManager.LocalPlayer.FightingTarget == null && !DynelManager.LocalPlayer.BlockAttack
                    && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                {
                    if (AttackBuddy.AttackBuddySettings["AggroTool"].AsBool() || AttackBuddy.AttackBuddySettings["Roam"].AsBool())
                    {
                        if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 4f
                            && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                            || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                        {
                            MovementController.Instance.Halt();
                            DynelManager.LocalPlayer.Attack(_target);
                            Chat.WriteLine($"Attacking {_target.Name}.");
                            _fightStartTime = Time.NormalTime;
                            return;
                        }
                        else if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                            && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
                        {
                            MovementController.Instance.Halt();
                            DynelManager.LocalPlayer.Attack(_target);
                            Chat.WriteLine($"Attacking {_target.Name}.");
                            _fightStartTime = Time.NormalTime;
                            return;
                        }
                        else if (!_target.IsMoving && _aggToolCounter >= 2
                            && _target.IsInLineOfSight
                            && _target.FightingTarget != null)
                        {
                            if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 4f
                                && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                                || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                            {
                                MovementController.Instance.SetDestination(_target.Position);
                            }
                            else if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                                && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
                            {
                                MovementController.Instance.SetDestination(_target.Position);
                            }
                        }
                    }
                    else
                    {
                        DynelManager.LocalPlayer.Attack(_target);
                        Chat.WriteLine($"Attacking {_target.Name}.");
                        _fightStartTime = Time.NormalTime;
                        return;
                    }
                }
                else
                {
                    if (_target.MaxHealth >= 1000000 || _target.Name == "Kyr'Ozch Maid" || _target.Name == "Kyr'Ozch Technician"
                        || _target.Name == "Defense Drone Tower" || _target.Name == "Control Leech" || _target.Name == "Kyr'Ozch Technician")
                    {
                        if (AttackBuddy._switchMobPrecision.Count >= 1)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null)
                            {
                                if (_target != AttackBuddy._switchMobPrecision.FirstOrDefault()
                                     && _target != AttackBuddy._switchMobCharging.FirstOrDefault() && _target != AttackBuddy._switchMobShield.FirstOrDefault())
                                {
                                    if (AttackBuddy._switchMobPrecision.FirstOrDefault().Health == 0) { return; }

                                    _target = AttackBuddy._switchMobPrecision.FirstOrDefault();
                                    DynelManager.LocalPlayer.Attack(_target);
                                    Chat.WriteLine($"Switching to target {_target.Name}.");
                                    _fightStartTime = Time.NormalTime;
                                    return;
                                }
                            }
                        }
                        else if (AttackBuddy._switchMobCharging.Count >= 1)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null)
                            {
                                if (_target != AttackBuddy._switchMobPrecision.FirstOrDefault()
                                    && _target != AttackBuddy._switchMobCharging.FirstOrDefault() && _target != AttackBuddy._switchMobShield.FirstOrDefault())
                                {
                                    if (AttackBuddy._switchMobCharging.FirstOrDefault().Health == 0) { return; }

                                    _target = AttackBuddy._switchMobCharging.FirstOrDefault();
                                    DynelManager.LocalPlayer.Attack(_target);
                                    Chat.WriteLine($"Switching to target {_target.Name}.");
                                    _fightStartTime = Time.NormalTime;
                                    return;
                                }
                            }
                        }
                        else if (AttackBuddy._switchMobShield.Count >= 1)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null)
                            {
                                if (_target != AttackBuddy._switchMobPrecision.FirstOrDefault()
                                       && _target != AttackBuddy._switchMobCharging.FirstOrDefault() && _target != AttackBuddy._switchMobShield.FirstOrDefault())
                                {
                                    if (AttackBuddy._switchMobShield.FirstOrDefault().Health == 0) { return; }

                                    _target = AttackBuddy._switchMobShield.FirstOrDefault();
                                    DynelManager.LocalPlayer.Attack(_target);
                                    Chat.WriteLine($"Switching to target {_target.Name}.");
                                    _fightStartTime = Time.NormalTime;
                                    return;
                                }
                            }
                        }
                        else if (AttackBuddy._switchMob.Count >= 1)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null)
                            {
                                if (AttackBuddy._switchMob.FirstOrDefault().Health == 0) { return; }

                                _target = AttackBuddy._switchMob.FirstOrDefault();
                                DynelManager.LocalPlayer.Attack(_target);
                                Chat.WriteLine($"Switching to target {_target.Name}.");
                                _fightStartTime = Time.NormalTime;
                                return;
                            }
                        }
                        else if (AttackBuddy._mob.Count >= 1)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null)
                            {
                                if (AttackBuddy._mob.FirstOrDefault().Health == 0) { return; }

                                _target = AttackBuddy._mob.FirstOrDefault();
                                DynelManager.LocalPlayer.Attack(_target);
                                Chat.WriteLine($"Switching to target {_target.Name}.");
                                _fightStartTime = Time.NormalTime;
                                return;
                            }
                        }
                    }
                    else if (AttackBuddy._switchMob.Count >= 1 && _target.Name != AttackBuddy._switchMob.FirstOrDefault().Name)
                    {
                        if (DynelManager.LocalPlayer.FightingTarget != null)
                        {
                            if (AttackBuddy._switchMob.FirstOrDefault().Health == 0) { return; }

                            _target = AttackBuddy._switchMob.FirstOrDefault();
                            DynelManager.LocalPlayer.Attack(_target);
                            Chat.WriteLine($"Switching to target {_target.Name}.");
                            _fightStartTime = Time.NormalTime;
                            return;
                        }
                    }
                }
            }

            if (_target.IsInLineOfSight
                && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending
                && !AttackBuddy.AttackBuddySettings["Attack"].AsBool()
                && AttackBuddy.AttackBuddySettings["AggroTool"].AsBool())
            {
                if (AttackBuddy.GetLeader() != null)
                {
                    if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                        && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
                    {
                        if (_aggToolCounter >= 5)
                        {
                            _ignoreTargetIdentity.Add(_target.Identity.Instance);
                        }
                        else if (Inventory.Find(83920, 83919, out Item aggroTool))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                aggroTool.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(244655, 244655, out Item scorpioTool))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                scorpioTool.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(83919, 83919, out Item aggroMultiTool)) // To-do could be wrong
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                aggroMultiTool.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(253186, 253186, out Item EmertoLow))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                EmertoLow.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(253187, 253187, out Item EmertoHigh))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                EmertoHigh.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                    }

                    if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 4f
                        && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                        || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                    {
                        if (_aggToolCounter >= 5)
                        {
                            _ignoreTargetIdentity.Add(_target.Identity.Instance);
                        }
                        else if (Inventory.Find(83920, 83919, out Item aggroTool))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                aggroTool.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(244655, 244655, out Item scorpioTool))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                scorpioTool.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(83919, 83919, out Item aggroMultiTool)) // To-do could be wrong
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                aggroMultiTool.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(253186, 253186, out Item EmertoLow))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                EmertoLow.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                        else if (Inventory.Find(253187, 253187, out Item EmertoHigh))
                        {
                            if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                            {
                                EmertoHigh.Use(_target, true);
                                _aggToolCounter++;
                                return;
                            }
                        }
                    }
                }
            }

            if (MovementController.Instance.IsNavigating && _target.IsInLineOfSight
                && !AttackBuddy.AttackBuddySettings["Attack"].AsBool()
                && !AttackBuddy.AttackBuddySettings["AggroTool"].AsBool())
            {
                if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 4f
                    && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                    || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                {
                    MovementController.Instance.Halt();
                }
                if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                    && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
                {
                    MovementController.Instance.Halt();
                }
            }

            if (!_target.IsMoving &&
                _target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 4f 
                && !AttackBuddy.AttackBuddySettings["Attack"].AsBool()
                && !AttackBuddy.AttackBuddySettings["AggroTool"].AsBool()
                && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                    || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
            {
                MovementController.Instance.SetDestination(_target.Position);
            }

            if (!_target.IsMoving &&
                _target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f 
                && !AttackBuddy.AttackBuddySettings["Attack"].AsBool()
                && !AttackBuddy.AttackBuddySettings["AggroTool"].AsBool()
                && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
            {
                MovementController.Instance.SetDestination(_target.Position);
            }
        }
    }

    [Flags]
    public enum CharacterWieldedWeapon
    {
        Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
        MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
        Melee = 0x02,             // 0x00000000000000000010b
        Ranged = 0x04,            // 0x00000000000000000100b
        Bow = 0x08,               // 0x00000000000000001000b
        Smg = 0x10,               // 0x00000000000000010000b
        Edged1H = 0x20,           // 0x00000000000000100000b
        Blunt1H = 0x40,           // 0x00000000000001000000b
        Edged2H = 0x80,           // 0x00000000000010000000b
        Blunt2H = 0x100,          // 0x00000000000100000000b
        Piercing = 0x200,         // 0x00000000001000000000b
        Pistol = 0x400,           // 0x00000000010000000000b
        AssaultRifle = 0x800,     // 0x00000000100000000000b
        Rifle = 0x1000,           // 0x00000001000000000000b
        Shotgun = 0x2000,         // 0x00000010000000000000b
        Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
        MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
        RangedEnergy = 0x10000,   // 0x00010000000000000000b
        Grenade2 = 0x20000,        // 0x00100000000000000000b
        HeavyWeapons = 0x40000,   // 0x01000000000000000000b
    }
}
