﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class DefendSpiritState : PositionHolder, IState
    {
        private SimpleChar _target;
        private SimpleChar _charmMob;

        private static bool _charMobAttacked = false;

        private static double _charmMobAttacking;

        private List<Identity> _charmMobs = new List<Identity>();

        private double _mobStuckStartTime;
        public const double MobStuckTimeout = 1500f;

        public DefendSpiritState() : base(Constants.DefendPos, 3f, 1)
        {

        }

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance != Constants.InfernoId && Playfield.ModelIdentity.Instance != Constants.NewInfMissionId
                 && Playfield.ModelIdentity.Instance != Constants.OmniPandeGId && Playfield.ModelIdentity.Instance != Constants.ClanPandeGId)
                return new IdleState();

            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (DynelManager.Exists(Constants.PlayerKillers))
            {
                foreach (Mission mission in Mission.List)
                {
                    if (mission.DisplayName.Contains("The Purification"))
                        mission.Delete();
                }

                InfBuddy.Trigger = false;

                Chat.WriteLine("Mission has failed, reforming");


                return new ExitMissionState();
            }

            if (!Mission.List.Any(x => x.DisplayName.Contains("The Purification")) && !InfBuddy.Config.DoubleReward)
            {
                Chat.WriteLine($"Mission finished.");
                return new ExitMissionState();
            }

            if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Me...")) && !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Ha...")) && InfBuddy.Config.DoubleReward && !Team.IsLeader)
            {
                if (InfBuddy.Trigger == true)
                    InfBuddy.Trigger = false;
                else
                    InfBuddy.Trigger = true;

                return new ExitMissionState();
            }

            if (Time.NormalTime > _mobStuckStartTime + MobStuckTimeout)
            {
                return new ExitMissionState();
            }

            if (_target != null)
                return new FightState(_target);

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("DefendSpiritState::OnStateEnter");

            _mobStuckStartTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("DefendSpiritState::OnStateExit");
        }

        private bool Rooted()
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot)) { return true; }

            return false;
        }

        public void Tick()
        {
            SimpleChar mob = DynelManager.NPCs
                    .Where(x => x.Name != Constants.SpiritNPCName
                        && x.Name != Constants.QuestStarterName
                        && x.Name != Constants.QuestStarterName)
                    .Where(x => !_charmMobs.Contains(x.Identity))
                    .Where(x => x.IsAlive && x.Position.DistanceFrom(InfBuddy.GetLeader().Position) <= 33f
                        && x.Position.DistanceFrom(Constants.DefendPos) <= 33f)
                    .OrderBy(x => x.Position.DistanceFrom(InfBuddy.GetLeader().Position))
                    .FirstOrDefault();

            _charmMob = DynelManager.NPCs
                    .Where(c => c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short))
                    .FirstOrDefault();

            if (_charmMob != null)
            {
                if (!_charmMobs.Contains(_charmMob.Identity))
                    _charmMobs.Add(_charmMob.Identity);

                if (Time.NormalTime - _charmMobAttacking > 8
                    && _charMobAttacked == true)
                {
                    _charMobAttacked = false;
                    _charmMobs.Remove(_charmMob.Identity);
                    _target = _charmMob;
                    Chat.WriteLine($"Found target: {_target.Name}.");
                }

                if (_charmMob.FightingTarget != null && _charmMob.IsAttacking
                    && _charmMobs.Contains(_charmMob.Identity)
                    && Team.Members.Select(c => c.Identity).Any(x => _charmMob.FightingTarget.Identity == x)
                    && _charMobAttacked == false)
                {
                    _charmMobAttacking = Time.NormalTime;
                    _charMobAttacked = true;
                }
            }

            if (mob != null)
            {
                _target = mob;

                Chat.WriteLine($"Found target: {_target.Name}");
            }
            else if (mob == null && DynelManager.LocalPlayer.HealthPercent >= 66 && DynelManager.LocalPlayer.NanoPercent >= 66
                    && DynelManager.LocalPlayer.MovementState != MovementState.Sit && !Rooted())
            {
                HoldPosition();
            }
        }
    }
}
