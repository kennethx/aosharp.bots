﻿using AOSharp.Common.GameData;

namespace InfBuddy
{
    public static class Constants
    {
        public const string SpiritNPCName = "Guardian Spirit of Purification";
        public const string QuestStarterName = "One Who Obeys Precepts";
        public const string Techno = "Buckethead Technodealer";
        public const string QuestGiverName = "The Retainer Of Ergo";
        public const string PlayerKillers = "Umbral Spectre";
        //public static Vector3 DefendPos = new Vector3(173.1f, 1.0f, 181.1f);
        public static Vector3 DefendPos = new Vector3(165.6f, 2.2f, 186.1f); // 169.3f, 1.0f, 171.5f
        public static Vector3 SpiritPos = new Vector3(165.6f, 2.2f, 186.1f);
        public static Vector3 WallBugFixSpot = new Vector3(2724.3f, 24.6f, 3325.6f);
        public static Vector3 ErgoVicinity = new Vector3(2791, 24.6f, 3360);
        public static Vector3 EntrancePos = new Vector3(2722.7f, 25.3f, 3333.2f);
        public static Vector3 EntranceFinalPos = new Vector3(2729.4f, 25.5f, 3345.2f);
        public static Vector3 ExitPos = new Vector3(162.1f, 2.3f, 105.3f);
        public static Vector3 ExitFinalPos = new Vector3(155.5f, 2.3f, 95.5f);
        public static Vector3 QuestGiverPos = new Vector3(2803.9f, 25.5f, 3375.2f); // 2806.5f, 25.5f, 3377.0f
        public static Vector3 QuestStarterBeforePos = new Vector3(168.9f, 1, 134.4f);
        public static Vector3 QuestStarterPos = new Vector3(181.5f, 1, 160.5f);
        public static Vector3 LeechSpot = new Vector3(155.5f, 2.7f, 99.6f); // old - 159.8f, 14.7f, 101.5f   new - 155.5f, 2.7f, 99.6f
        public static Vector3 LeechMissionExit = new Vector3(161.2f, 2.7f, 104.2f);
        public const int InfernoId = 4605;
        public const int OmniPandeGId = 4697;
        public const int ClanPandeGId = 4696;
        public const int PandePlatId = 4328;
        public const int NewInfMissionId = 9042;
    }
}
