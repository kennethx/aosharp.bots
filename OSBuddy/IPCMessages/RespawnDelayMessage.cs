﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace OSBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.RespawnDelay)]
    public class RespawnDelayMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.RespawnDelay;

        [AoMember(0)]
        public int Delay { get; set; }
    }
}
