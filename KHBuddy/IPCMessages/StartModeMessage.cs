﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartMode)]
    public class StartModeMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartMode;

        [AoMember(0)]
        public bool West { get; set; }
        [AoMember(1)]
        public bool East { get; set; }
        [AoMember(2)]
        public bool Beach { get; set; }

        [AoMember(3)]
        public bool BothSides { get; set; }
    }
}
