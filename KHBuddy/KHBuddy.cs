﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using KHBuddy.IPCMessages;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;
using AOSharp.Common.GameData.UI;

namespace KHBuddy
{
    public class KHBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static bool Running = false;

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public static string PluginDirectory;


        public static Settings KHBuddySettings = new Settings("KHBuddy");

        public static bool FollowerSwitch = false;
        public static bool MoveToStartEast = false;
        public static bool MoveToStartWest = false;

        public static bool KHWest = false;
        public static bool KHEast = false;

        public static DateTime RespawnTime;
        public static DateTime RespawnTimeWest;
        public static DateTime RespawnTimeEast;
        public static DateTime GameTime;

        public static double _stateTimeOut = Time.NormalTime;

        public static bool SwitchingSides = false;

        public override void Run(string pluginDir)
        {
            try
            {
                PluginDirectory = pluginDir;

                Chat.WriteLine("KHBuddy Loaded!");
                Chat.WriteLine("/khbuddy for settings.");

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\KHBuddy\\{Game.ClientInst}\\Config.json");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

                KHBuddySettings.AddVariable("Running", false);

                KHBuddySettings.AddVariable("West", false);
                KHBuddySettings.AddVariable("East", false);
                KHBuddySettings.AddVariable("Beach", false);
                KHBuddySettings.AddVariable("BothSides", false);

                KHBuddySettings.AddVariable("Leash", true);

                KHBuddySettings["Running"] = false;
                KHBuddySettings["BothSides"] = false;

                SettingsController.RegisterSettingsWindow("KHBuddy", pluginDir + "\\UI\\KHBuddySettingWindow.xml", KHBuddySettings);

                _stateMachine = new StateMachine(new IdleState());

                IPCChannel.RegisterCallback((int)IPCOpcode.StartMode, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopMode, OnStopMessage);


                Chat.RegisterCommand("buddy", KHBuddyCommand);

                Game.OnUpdate += OnUpdate;
            }
            catch(Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            Leader = new Identity(IdentityType.SimpleChar, sender);

            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            StartModeMessage startMsg = (StartModeMessage)msg;

            KHBuddySettings["West"] = startMsg.West;
            KHBuddySettings["East"] = startMsg.East;
            KHBuddySettings["Beach"] = startMsg.Beach;
            KHBuddySettings["BothSides"] = startMsg.BothSides;

            Start();
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            StopModeMessage stopMsg = (StopModeMessage)msg;

            Running = false;

            KHBuddySettings["West"] = stopMsg.West;
            KHBuddySettings["East"] = stopMsg.East;
            KHBuddySettings["Beach"] = stopMsg.Beach;
            KHBuddySettings["BothSides"] = stopMsg.BothSides;

            KHBuddySettings["Running"] = false;

            if (NavMeshMovementController != null)
                NavMeshMovementController.Halt();
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        private void Start()
        {
            Running = true;

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer && !(_stateMachine.CurrentState is PullState))
                _stateMachine.SetState(new PullState());

            if (DynelManager.LocalPlayer.Profession == Profession.NanoTechnician && !(_stateMachine.CurrentState is NukeState))
                _stateMachine.SetState(new NukeState());
        }

        private void Stop()
        {
            Running = false;

            KHBuddySettings["Running"] = false;

            _stateMachine.SetState(new IdleState());

            if (NavMeshMovementController != null)
                NavMeshMovementController.Halt();
        }

        private void HelpBox(object s, ButtonBase button)
        {
            Window helpWindow = Window.CreateFromXml("Help", PluginDirectory + "\\UI\\KHBuddyHelpBox.xml",
            windowSize: new Rect(0, 0, 455, 345),
            windowStyle: WindowStyle.Default,
            windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);
            helpWindow.Show(true);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            GameTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Time.NormalTime);

            if (Game.IsZoning)
                return;

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                if (KHBuddySettings["East"].AsBool())
                {
                    Debug.DrawSphere(new Vector3(1115.9f, 1.6f, 1064.3f), 0.2f, DebuggingColor.White);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, new Vector3(1115.9f, 1.6f, 1064.3f), DebuggingColor.White); // East
                }

                if (KHBuddySettings["West"].AsBool())
                {
                    Debug.DrawSphere(new Vector3(1043.2f, 1.6f, 1021.1f), 0.2f, DebuggingColor.White);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, new Vector3(1043.2f, 1.6f, 1021.1f), DebuggingColor.White); // West
                }

                if (KHBuddySettings["Beach"].AsBool())
                {
                    Debug.DrawSphere(new Vector3(898.1f, 4.4f, 289.9f), 0.2f, DebuggingColor.White);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, new Vector3(898.1f, 4.4f, 289.9f), DebuggingColor.White); // beach
                }
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView textinput1);

                if (textinput1 != null && textinput1.Text != String.Empty)
                {
                    if (int.TryParse(textinput1.Text, out int channelValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        {
                            IPCChannel.SetChannelId(Convert.ToByte(channelValue));
                            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(channelValue);
                            SettingsController.KHBuddyChannel = channelValue.ToString();
                            Config.Save();
                        }
                    }
                }

                if (SettingsController.settingsView != null)
                {
                    if (SettingsController.settingsView.FindChild("KHBuddyHelpBox", out Button helpBox))
                    {
                        helpBox.Tag = SettingsController.settingsView;
                        helpBox.Clicked = HelpBox;
                    }
                }

                if (KHBuddySettings["Leash"].AsBool() && Config.CharSettings[Game.ClientInst].Leash != true)
                {
                    Config.CharSettings[Game.ClientInst].Leash = true;
                    Config.Save();
                    return;
                }
                if (!KHBuddySettings["Leash"].AsBool() && Config.CharSettings[Game.ClientInst].Leash == true)
                {
                    Config.CharSettings[Game.ClientInst].Leash = false;
                    Config.Save();
                    return;
                }

                if (!KHBuddySettings["Running"].AsBool() && Running == true)
                {
                    IPCChannel.Broadcast(new StopModeMessage()
                    {
                        West = KHBuddySettings["West"].AsBool(),
                        East = KHBuddySettings["East"].AsBool(),
                        Beach = KHBuddySettings["Beach"].AsBool(),
                        BothSides = KHBuddySettings["BothSides"].AsBool()
                    });
                    Stop();
                    return;
                }
                if (KHBuddySettings["Running"].AsBool() && Running == false)
                {
                    if (KHBuddySettings["West"].AsBool() &&
                        KHBuddySettings["East"].AsBool() && KHBuddySettings["Beach"].AsBool())
                    {
                        KHBuddySettings["West"] = false;
                        KHBuddySettings["East"] = false;
                        KHBuddySettings["Beach"] = false;
                        KHBuddySettings["BothSides"] = false;
                        KHBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (KHBuddySettings["West"].AsBool() && KHBuddySettings["Beach"].AsBool()
                        || (KHBuddySettings["East"].AsBool() && KHBuddySettings["Beach"].AsBool()))
                    {
                        KHBuddySettings["West"] = false;
                        KHBuddySettings["East"] = false;
                        KHBuddySettings["Beach"] = false;
                        KHBuddySettings["BothSides"] = false;
                        KHBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (KHBuddySettings["West"].AsBool() && KHBuddySettings["East"].AsBool()
                        && !KHBuddySettings["Beach"].AsBool())
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;

                        KHBuddySettings["BothSides"] = true;

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 5f) // East
                            {
                                KHBuddySettings["West"] = false;

                                IPCChannel.Broadcast(new StartModeMessage()
                                {
                                    West = KHBuddySettings["West"].AsBool(),
                                    East = KHBuddySettings["East"].AsBool(),
                                    Beach = KHBuddySettings["Beach"].AsBool(),
                                    BothSides = KHBuddySettings["BothSides"].AsBool()
                                });
                            }

                            if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1021.1f)) < 5f) // West
                            {
                                KHBuddySettings["East"] = false;

                                IPCChannel.Broadcast(new StartModeMessage()
                                {
                                    West = KHBuddySettings["West"].AsBool(),
                                    East = KHBuddySettings["East"].AsBool(),
                                    Beach = KHBuddySettings["Beach"].AsBool(),
                                    BothSides = KHBuddySettings["BothSides"].AsBool()
                                });
                            }
                        }
                        Start();
                        return;
                    }
                    else
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;

                        KHBuddySettings["BothSides"] = false;

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StartModeMessage()
                            {
                                West = KHBuddySettings["West"].AsBool(),
                                East = KHBuddySettings["East"].AsBool(),
                                Beach = KHBuddySettings["Beach"].AsBool(),
                                BothSides = KHBuddySettings["BothSides"].AsBool()
                            });
                        }
                        Start();
                        return;
                    }

                }
            }

            if (SettingsController.KHBuddyChannel == String.Empty)
            {
                SettingsController.KHBuddyChannel = Config.IPCChannel.ToString();
            }

            _stateMachine.Tick();
        }

        private void KHBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!KHBuddySettings["West"].AsBool() && !KHBuddySettings["East"].AsBool()
                        && !KHBuddySettings["Beach"].AsBool())
                    {
                        KHBuddySettings["West"] = false;
                        KHBuddySettings["East"] = false;
                        KHBuddySettings["Beach"] = false;
                        KHBuddySettings["BothSides"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (KHBuddySettings["West"].AsBool() && KHBuddySettings["Beach"].AsBool()
                        || (KHBuddySettings["East"].AsBool() && KHBuddySettings["Beach"].AsBool()))
                    {
                        KHBuddySettings["West"] = false;
                        KHBuddySettings["East"] = false;
                        KHBuddySettings["Beach"] = false;
                        KHBuddySettings["BothSides"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (!KHBuddySettings["Running"].AsBool() && !Running)
                    {
                        if (KHBuddySettings["West"].AsBool() && KHBuddySettings["East"].AsBool()
                            && !KHBuddySettings["Beach"].AsBool())
                        {
                            IsLeader = true;
                            Leader = DynelManager.LocalPlayer.Identity;

                            KHBuddySettings["BothSides"] = true;

                            if (DynelManager.LocalPlayer.Identity == Leader)
                            {
                                if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 5f) // East
                                {
                                    KHBuddySettings["West"] = false;

                                    IPCChannel.Broadcast(new StartModeMessage()
                                    {
                                        West = KHBuddySettings["West"].AsBool(),
                                        East = KHBuddySettings["East"].AsBool(),
                                        Beach = KHBuddySettings["Beach"].AsBool(),
                                        BothSides = KHBuddySettings["BothSides"].AsBool()
                                    });
                                }

                                if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1021.1f)) < 5f) // West
                                {
                                    KHBuddySettings["East"] = false;

                                    IPCChannel.Broadcast(new StartModeMessage()
                                    {
                                        West = KHBuddySettings["West"].AsBool(),
                                        East = KHBuddySettings["East"].AsBool(),
                                        Beach = KHBuddySettings["Beach"].AsBool(),
                                        BothSides = KHBuddySettings["BothSides"].AsBool()
                                    });
                                }
                            }
                            Start();
                            Chat.WriteLine("Starting");
                            return;
                        }
                        else
                        {
                            IsLeader = true;
                            Leader = DynelManager.LocalPlayer.Identity;

                            KHBuddySettings["BothSides"] = false;

                            if (DynelManager.LocalPlayer.Identity == Leader)
                            {
                                IPCChannel.Broadcast(new StartModeMessage()
                                {
                                    West = KHBuddySettings["West"].AsBool(),
                                    East = KHBuddySettings["East"].AsBool(),
                                    Beach = KHBuddySettings["Beach"].AsBool(),
                                    BothSides = KHBuddySettings["BothSides"].AsBool()
                                });
                            }
                            Start();
                            Chat.WriteLine("Starting");
                            return;
                        }
                    }
                    else if (KHBuddySettings["Running"].AsBool() && Running)
                    {
                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StopModeMessage()
                            {
                                West = KHBuddySettings["West"].AsBool(),
                                East = KHBuddySettings["East"].AsBool(),
                                Beach = KHBuddySettings["Beach"].AsBool(),
                                BothSides = KHBuddySettings["BothSides"].AsBool()
                            });
                        }

                        Stop();
                        Chat.WriteLine("Stopping");
                        return;
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
