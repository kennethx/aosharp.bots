﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AXPBuddy
{
    public class EnterSectorState : IState
    {
        private const int MinWait = 5;
        private const int MaxWait = 14;
        private CancellationTokenSource _cancellationToken = new CancellationTokenSource();

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.S13Id)
            {
                if (AXPBuddy._settings["Toggle"].AsBool())
                {
                    if (AXPBuddy._settings["Leecher"].AsBool())
                        return new LeechState();
                    else
                        return new PatrolState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("EnterSectorState::OnStateEnter");

            //AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13EntrancePos);

            if (AXPBuddy._settings["Leecher"].AsBool() || AXPBuddy._settings["Merge"].AsBool())
            {
                Chat.WriteLine($"Idling for 16 seconds because we are a leecher or using Merge mode.");

                Task.Delay(16 * 1000).ContinueWith(x =>
                {
                    AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13EntrancePos);
                }, _cancellationToken.Token);
            }
            else if (AXPBuddy.IsLeader && !AXPBuddy._settings["Merge"].AsBool())
            {
                Chat.WriteLine($"Entering first because we are the leader.");

                Task.Delay(1 * 1000).ContinueWith(x =>
                {
                    AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13EntrancePos);
                }, _cancellationToken.Token);
            }
            else
            {
                int randomWait = Utils.Next(MinWait, MaxWait);
                Chat.WriteLine($"Idling for {randomWait} seconds..");

                Task.Delay(randomWait * 1000).ContinueWith(x =>
                {
                    AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13EntrancePos);
                }, _cancellationToken.Token);
            }
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("EnterSectorState::OnStateExit");
            _cancellationToken.Cancel();
        }

        public void Tick()
        {
            //if()
            //APFBuddy.MovementController.SetNavMeshDestination(APFBuddy.ActiveGlobalSettings.GetSectorEntrancePos());
        }
    }
}
