﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace AttackBuddy
{
    public class NukeState : IState
    {
        public SimpleChar LeaderChar;
        Spell aoenukes = null;

        public IState GetNextState()
        {
            if (Time.NormalTime - AttackBuddy._stateTimeOut > 3600f)
            {
                AttackBuddy.AttackBuddySettings["Nuke"] = false;
                Chat.WriteLine("Turning off bot, Idle for too long.");
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            AttackBuddy._stateTimeOut = Time.NormalTime;
            //Chat.WriteLine("NukeState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("NukeState::OnStateExit");
        }

        private static class RelevantNanos
        {
            public static readonly int[] AOENukes = { 28638,
                266297, 28637, 28594, 45922, 45906, 45884, 28635, 266298, 28593, 45925, 45940, 45900,28629,
                45917, 45937, 28599, 45894, 45943, 28633, 28631 };
        }

        public void Tick()
        {
            if (DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
            {
                if (aoenukes == null)
                    aoenukes = Spell.List.Where(x => RelevantNanos.AOENukes.Contains(x.Id)).FirstOrDefault();

                if (AttackBuddy.AttackBuddySettings["Nuke"].AsBool())
                {
                    List<SimpleChar> mobs = DynelManager.NPCs
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= SettingsController.AttackBuddyThreatRange)
                            .Where(x => x.IsAlive)
                            .Where(x => x.IsInLineOfSight)
                            .Where(x => !x.IsMoving)
                            .ToList();

                    if (!Spell.HasPendingCast && aoenukes.IsReady 
                        && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66)
                        && mobs.FirstOrDefault() != null)
                    {
                        aoenukes.Cast(mobs.FirstOrDefault(), true);
                        AttackBuddy._stateTimeOut = Time.NormalTime;
                    }
                }
            }
        }
    }
}
