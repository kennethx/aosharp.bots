﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Linq;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class LeechState : IState
    {
        private bool _missionsLoaded = false;
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.InfernoId)
                return new ReformState();

            if (_missionsLoaded && !Mission.List.Exists(x => x.DisplayName.Contains(InfBuddy.Config.CharSettings[Game.ClientInst].GetMissionName())) && !InfBuddy.Config.DoubleReward)
                return new ExitMissionState();

            if ((_missionsLoaded && !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Me...")) && !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Ha...")) && InfBuddy.Config.DoubleReward && !Team.IsLeader))
            {
                if (InfBuddy.Trigger == true)
                    InfBuddy.Trigger = false;
                else
                    InfBuddy.Trigger = true;

                return new ExitMissionState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("LeechState::OnStateEnter");

            Chat.WriteLine("Leecher will now move to leech spot.");

            DynelManager.LocalPlayer.Position = Constants.LeechSpot;
            MovementController.Instance.SetMovement(MovementAction.Update);
            MovementController.Instance.SetMovement(MovementAction.JumpStart);
            //MovementController.Instance.SetDestination(new Vector3(159.4f, 15.0f, 99.3f));
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("LeechState::OnStateExit");
            DynelManager.LocalPlayer.Position = new Vector3(160.4f, 2.6f, 103.0f);
        }

        public void Tick()
        {
            //Fix for check happening before missions are loaded on zone
            if (Mission.List.Exists(x => x.DisplayName.Contains(InfBuddy.Config.CharSettings[Game.ClientInst].GetMissionName())))
                _missionsLoaded = true;
            if (Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Me...")) && InfBuddy.Config.DoubleReward)
                _missionsLoaded = true;
        }
    }
}
