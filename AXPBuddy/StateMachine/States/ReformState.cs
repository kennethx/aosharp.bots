﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AXPBuddy.IPCMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages.OrgServerMessages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AXPBuddy
{
    public class ReformState : IState
    {
        private bool _invitedPastTeamMembers = false;
        private bool _init = false;

        private double _reformStartedTime;
        private double _gracePeriod;

        private ReformPhase _phase;
        public static Identity[] _teamCache;
        private List<Identity> _alreadyInvitedCharacters = new List<Identity>();

        public static ConcurrentQueue<Identity> LFT = new ConcurrentQueue<Identity>();

        public IState GetNextState()
        {
            //Non-leaders will have to wait for a state change from the leader to leave this state.

            if (AXPBuddy.IsLeader && _phase == ReformPhase.AwaitingTeammembers)
            {
                if (Time.NormalTime > _reformStartedTime + 180f)
                {
                    Chat.WriteLine($"Reform timed out, retrying");
                    AXPBuddy._stateTimeOut = Time.NormalTime;
                    return new ReformState();
                }

                if (Team.Members.Count == _teamCache.Length)
                {
                    if (Time.NormalTime > _gracePeriod + 22f)
                    {
                        Chat.WriteLine($"Reform complete.");
                        AXPBuddy.IPCChannel.Broadcast(new StartMessage());
                        return new IdleState();
                    }
                }
            }

            if (_phase == ReformPhase.Merge)
            {
                if (Team.Members.Count == _teamCache.Length)
                {
                    if (Time.NormalTime > _gracePeriod + 45f)
                    {
                        Chat.WriteLine($"Reform complete.");
                        AXPBuddy.IPCChannel.Broadcast(new StartMessage());
                        return new IdleState();
                    }
                }
            }

            return null;
        }
        private void Team_TeamRequest(object s, TeamRequestEventArgs e)
        {
            if (_teamCache.Contains(e.Requester))
            {
                e.Accept();
            }
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("ReformState::OnStateEnter");

            _teamCache = Team.Members.Select(x => x.Identity).ToArray();

            if (AXPBuddy._settings["Merge"].AsBool())
            {
                Team.TeamRequest += Team_TeamRequest;
                _phase = ReformPhase.Merge;
            }
            else
            {
                _phase = ReformPhase.MissionExitBuffer;
                _reformStartedTime = Time.NormalTime;
            }
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("ReformState::OnStateExit");

            if (AXPBuddy._settings["Merge"].AsBool())
            {
                Team.TeamRequest -= Team_TeamRequest;
                _init = false;
            }

            if (Team.IsLeader)
            {
                Team.ConvertToRaid();
            }
        }

        public void Tick()
        {
            if (!AXPBuddy.IsLeader)
                return;

            if (_phase == ReformPhase.MissionExitBuffer && Time.NormalTime > _reformStartedTime + 5)
            {
                Team.Disband();
                _phase = ReformPhase.Disbanding;
                _gracePeriod = Time.NormalTime;
                //Chat.WriteLine("ReformPhase.Disbanding");
            }

            if (_phase == ReformPhase.Merge && !_init)
            {
                _init = true;
                _gracePeriod = Time.NormalTime;
                //Chat.WriteLine("ReformPhase.Disbanding");
            }

            if (_phase == ReformPhase.Disbanding && !Team.IsInTeam && Time.NormalTime > _gracePeriod + 40f)
            {
                foreach (SimpleChar player in DynelManager.Players.Where(x => _teamCache.Contains(x.Identity) && x.IsInPlay))
                    InvitePlayer(player.Identity);

                _phase = ReformPhase.AwaitingTeammembers;
                //Chat.WriteLine("ReformPhase.AwaitingTeammembers");
            }

            if (_phase == ReformPhase.AwaitingTeammembers && !_invitedPastTeamMembers && Time.NormalTime > _reformStartedTime + 15)
            {
                foreach (Identity oldTeammate in _teamCache)
                    InvitePlayer(oldTeammate);

                _invitedPastTeamMembers = true;
            }
        }

        private void InvitePlayer(Identity player)
        {
            if (player == DynelManager.LocalPlayer.Identity || Team.Members.Any(j => j.Identity == player) || _alreadyInvitedCharacters.Any(j => j == player))
                return;

            Team.Invite(player);
            Chat.WriteLine($"Inviting {player}");

            _alreadyInvitedCharacters.Add(player);
        }

        //private void OnDynelSpawned(object s, Dynel dynel)
        //{
        //    if (_phase != ReformPhase.AwaitingTeammembers)
        //        return;

        //    if (!_teamCache.Contains(dynel.Identity))
        //        return;

        //    SimpleChar oldTeammate = dynel.Cast<SimpleChar>();

        //    if (!oldTeammate.IsInPlay)
        //        return;

        //    InvitePlayer(oldTeammate.Identity);
        //}

        //private void OnCharInPlay(object s, SimpleChar character)
        //{
        //    if (_phase != ReformPhase.AwaitingTeammembers)
        //        return;

        //    if (!_teamCache.Contains(character.Identity))
        //        return;

        //    InvitePlayer(character.Identity);
        //}

        private enum ReformPhase
        {
            MissionExitBuffer,
            Disbanding,
            AwaitingTeammembers,
            Merge
        }
    }
}
