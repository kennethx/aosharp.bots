﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace AttackBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.NoneSelected)]
    public class NoneSelectedMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.NoneSelected;
    }
}
