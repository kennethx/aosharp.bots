﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace InfBuddy
{
    public class FightState : PositionHolder, IState
    {
        public const double FightTimeout = 60f;
        private SimpleChar _target;
        private double _fightStartTime;

        public FightState(SimpleChar target) : base(Constants.DefendPos, 3f, 1)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
                return new DiedState();

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
                return new DiedState();

            if (!_target.IsValid ||
               !_target.IsAlive ||
               Time.NormalTime > _fightStartTime + FightTimeout)
            {
                return new DefendSpiritState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("FightState::OnStateEnter");
            _fightStartTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("FightState::OnStateExit");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }

        private bool IsBeingAttacked()
        {
            SimpleChar mobs = DynelManager.NPCs
                .Where(c => c.FightingTarget != null)
                .Where(c => c.FightingTarget.Name != "Guardian Spirit of Purification")
                .FirstOrDefault();

            if (mobs != null) { return true; }

            return false;
        }

        public static CharacterWieldedWeapon GetWieldedWeapons(SimpleChar local) => (CharacterWieldedWeapon)local.GetStat(Stat.EquippedWeapons);

        public void Tick()
        {
            if (!_target.IsValid)
                return;

            if (_target.IsInLineOfSight && _target.IsInAttackRange() && DynelManager.LocalPlayer.IsAttackPending 
                && DynelManager.LocalPlayer.IsAttacking)
                return;

            if (_target.IsInLineOfSight && _target.IsInAttackRange() && !DynelManager.LocalPlayer.IsAttackPending 
                && !DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.Attack(_target);
            }

            if (!InfBuddy.NavMeshMovementController.IsNavigating && !_target.IsInLineOfSight)
            {
                InfBuddy.NavMeshMovementController.SetNavMeshDestination(_target.Position);
            }

            if (InfBuddy.NavMeshMovementController.IsNavigating && _target.IsInLineOfSight
                    && InfBuddy.Config.CharSettings[Game.ClientInst].PathToMob)
            {
                if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 4f
                    && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                    || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
                {
                    InfBuddy.NavMeshMovementController.Halt();
                }
                if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= 11f
                    && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
                {
                    InfBuddy.NavMeshMovementController.Halt();
                }
            }

            if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 4f
                    && InfBuddy.Config.CharSettings[Game.ClientInst].PathToMob
                    && (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee)
                    || GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.MartialArts)))
            {
                InfBuddy.NavMeshMovementController.SetNavMeshDestination(_target.Position);
            }

            if (_target.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 11f
                    && InfBuddy.Config.CharSettings[Game.ClientInst].PathToMob
                    && GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
            {
                InfBuddy.NavMeshMovementController.SetNavMeshDestination(_target.Position);
            }

            if (_target == null && !IsBeingAttacked())
                HoldPosition();
        }
    }
    public enum CharacterWieldedWeapon
    {
        Fists = 0x0,            // 0x00000000000000000000b Fists / invalid
        MartialArts = 0x01,             // 0x00000000000000000001b martialarts / fists
        Melee = 0x02,             // 0x00000000000000000010b
        Ranged = 0x04,            // 0x00000000000000000100b
        Bow = 0x08,               // 0x00000000000000001000b
        Smg = 0x10,               // 0x00000000000000010000b
        Edged1H = 0x20,           // 0x00000000000000100000b
        Blunt1H = 0x40,           // 0x00000000000001000000b
        Edged2H = 0x80,           // 0x00000000000010000000b
        Blunt2H = 0x100,          // 0x00000000000100000000b
        Piercing = 0x200,         // 0x00000000001000000000b
        Pistol = 0x400,           // 0x00000000010000000000b
        AssaultRifle = 0x800,     // 0x00000000100000000000b
        Rifle = 0x1000,           // 0x00000001000000000000b
        Shotgun = 0x2000,         // 0x00000010000000000000b
        Grenade = 0x8000,     // 0x00000100000000000000b // 0x00001000000000000000b grenade / martial arts
        MeleeEnergy = 0x4000,     // 0x00001000000000000000b // 0x00000100000000000000b
        RangedEnergy = 0x10000,   // 0x00010000000000000000b
        Grenade2 = 0x20000,        // 0x00100000000000000000b
        HeavyWeapons = 0x40000,   // 0x01000000000000000000b
    }
}
