﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (OSBuddy.Running == true)
                return new PullState();

            return null;
        }

        public void OnStateEnter()
        {
            //Chat.WriteLine("IdleState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("IdleState::OnStateExit");
        }

        public void Tick()
        {
            if (OSBuddy.RoamingVectors.Count >= 1)
            {
                foreach (Vector3 pos in OSBuddy.RoamingVectors)
                {
                    Debug.DrawSphere(pos, 0.2f, DebuggingColor.White);
                }
            }
        }
    }
}
