﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using InfBuddy.IPCMessages;
using AOSharp.Common.GameData.UI;

namespace InfBuddy
{
    public class InfBuddy : AOPluginEntry
    {
        private StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; set; }

        public static Config Config { get; private set; }

        public static bool Toggle = false;
        public static bool _firstTimeout = false;
        public static bool _secondTimeout = false;

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public static bool Trigger = false;

        public static string PluginDirectory;

        public static double _stateTimeOut = Time.NormalTime;
        public static double CorrectionPathTimer = 0;

        public static Window infoWindow;
        //public static Window researchWindow;

        //public static int _currentLine = 0;
        //public static int _lineModifier = 1;

        public static Settings _settings = new Settings("InfBuddy");
        //public static Settings _research = new Settings("Research");

        //public static List<int> _researchLinesActive = new List<int>();

        //#region Research

        //public static Dictionary<Profession, Dictionary<PerkLine, int>> _researchMapHashs = new Dictionary<Profession, Dictionary<PerkLine, int>>
        //{
        //    {
        //                Profession.Soldier, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.SweepandClear, 2790 },
        //                    { PerkLine.StrategicPlanning, 2800 },
        //                    { PerkLine.CombatSense, 2810 },
        //                    { PerkLine.Marksmanship, 2820 },
        //                    { PerkLine.ForwardObserver, 2830 },
        //                    { PerkLine.ClassifiedOps, 2840 },
        //                    { PerkLine.ForceRecon, 2850 },
        //                }
        //    },
        //    {
        //                Profession.Bureaucrat, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.HumanResources, 2160 },
        //                    { PerkLine.TeamBuilding, 2170 },
        //                    { PerkLine.ProcessTheory, 2180 },
        //                    { PerkLine.ExecutiveDecisions, 2190 },
        //                    { PerkLine.HostileNegotiations, 2200 },
        //                    { PerkLine.MarketAwareness, 2210 }
        //                }
        //    },
        //    {
        //                Profession.NanoTechnician, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.IntellectualRefinement, 2720 },
        //                    { PerkLine.NanoTheory, 2730 },
        //                    { PerkLine.ParticlePhysics, 2740 },
        //                    { PerkLine.CombatExecution, 2750 },
        //                    { PerkLine.PracticalUse, 2760 },
        //                    { PerkLine.KineticMastery, 2770 }
        //                }
        //    },
        //    {
        //                Profession.MartialArtist, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.Intuition, 2580 },
        //                    { PerkLine.Meditation, 2590 },
        //                    { PerkLine.Empathy, 2600 },
        //                    { PerkLine.Nimble, 2610 },
        //                    { PerkLine.Alacrity, 2620 },
        //                    { PerkLine.Reflex, 2630 },
        //                    { PerkLine.Cognizance, 2640 }
        //                }
        //    },
        //    {
        //                Profession.Shade, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.AssassinsAwareness, 2860 },
        //                    { PerkLine.HonedSenses, 2870 },
        //                    { PerkLine.KillingBlows, 2880 },
        //                    { PerkLine.StilettoMastery, 2890 },
        //                    { PerkLine.MaliciousForethought, 2900 },
        //                    { PerkLine.Lithe, 2900 },
        //                    { PerkLine.Ambushing, 2910 }
        //                }
        //    },
        //    {
        //                Profession.Doctor, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.BedsideManner, 2230 },
        //                    { PerkLine.Rehabilitation, 2240 },
        //                    { PerkLine.Diagnosis, 2250 },
        //                    { PerkLine.Internship, 2260 },
        //                    { PerkLine.UndergroundDoctor, 2270 },
        //                    { PerkLine.Toxicology, 2280 },
        //                    { PerkLine.AggressiveSurgery, 2290 }
        //                }
        //    },
        //    {
        //                Profession.Enforcer, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.Endurance, 2300 },
        //                    { PerkLine.Flexibility, 2310 },
        //                    { PerkLine.BrawlersSense, 2320 },
        //                    { PerkLine.Kneecapping, 2330 },
        //                    { PerkLine.AngerManagement, 2340 },
        //                    { PerkLine.HardLabor, 2350 },
        //                    { PerkLine.Brutality, 2360 }
        //                }
        //    },
        //    {
        //                Profession.Fixer, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.Acquisition, 2440 },
        //                    { PerkLine.Subtlety, 2450 },
        //                    { PerkLine.Cunning, 2460 },
        //                    { PerkLine.SmugglersSense, 2470 },
        //                    { PerkLine.RespectableBusinessman, 2480 },
        //                    { PerkLine.FallbackPlan, 2490 },
        //                    { PerkLine.Insurance, 2500 }
        //                }
        //    },
        //    {
        //                Profession.Trader, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.EyeforaDeal, 2930 },
        //                    { PerkLine.FastTalk, 2940 },
        //                    { PerkLine.SensibleInvestment, 2950 },
        //                    { PerkLine.AggressivePricing, 2960 },
        //                    { PerkLine.DoorToDoorSalesman, 2970 },
        //                    { PerkLine.SensitiveNegotiations, 2980 },
        //                    { PerkLine.HostileTakeover, 2990 }
        //                }
        //    },
        //    {
        //                Profession.Keeper, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.Champion, 2510 },
        //                    { PerkLine.Exemplar, 2520 },
        //                    { PerkLine.Wisdom, 2530 },
        //                    { PerkLine.Loyalty, 2540 },
        //                    { PerkLine.Judgement, 2550 },
        //                    { PerkLine.Virtue, 2560 },
        //                    { PerkLine.Paragon, 2570 }
        //                }
        //    },
        //    {
        //                Profession.Engineer, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.PracticalApplication, 2370 },
        //                    { PerkLine.Serendipity, 2380 },
        //                    { PerkLine.MechanicalAssistance, 2390 },
        //                    { PerkLine.ProcessRefinement, 2400 },
        //                    { PerkLine.Ergonomics, 2410 },
        //                    { PerkLine.MilitaryHardware, 2420 },
        //                    { PerkLine.CombatApplications, 2430 }
        //                }
        //    },
        //    {
        //                Profession.Agent, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.EndCertification, 2090 },
        //                    { PerkLine.ThreatAssessment, 2100 },
        //                    { PerkLine.DirectAction, 2120 },
        //                    { PerkLine.Fitness, 2130 },
        //                    { PerkLine.Intuition, 2140 },
        //                    { PerkLine.Stealth, 2150 },
        //                    { PerkLine.Marksmanship, 2160 }
        //                }
        //    },
        //    {
        //                Profession.Metaphysicist, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.Foresight, 2650 },
        //                    { PerkLine.Sympathy, 2660 },
        //                    { PerkLine.SpatialAwareness, 2670 },
        //                    { PerkLine.Trauma, 2680 },
        //                    { PerkLine.Perseverences, 2690 },
        //                    { PerkLine.Jealousy, 2700 },
        //                    { PerkLine.Angst, 2710 }
        //                }
        //    },
        //    {
        //                Profession.Adventurer, new Dictionary<PerkLine, int>
        //                {
        //                    { PerkLine.Exploration, 2020 },
        //                    { PerkLine.KeenEyes, 2030 },
        //                    { PerkLine.WildernessLore, 2040 },
        //                    { PerkLine.Gunslinger, 2050 },
        //                    { PerkLine.WildernessSurvival, 2060 },
        //                    { PerkLine.GameWarden, 2070 },
        //                    { PerkLine.SafariGuide, 2090 }
        //                }
        //    },
        //};

        //#endregion

        public override void Run(string pluginDir)
        {
            try
            {
                PluginDirectory = pluginDir;

                Chat.WriteLine("InfBuddy Loaded!");
                Chat.WriteLine("/infbuddy for settings.");

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\InfBuddy\\{Game.ClientInst}\\Config.json");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

                _settings.AddVariable("Toggle", false);

                _settings.AddVariable("Leecher", false);
                _settings.AddVariable("DoubleReward", false);
                _settings.AddVariable("Pathing", false);

                //_research.AddVariable("Toggle", false);

                //_research.AddVariable("Line1", false);
                //_research.AddVariable("Line2", false);
                //_research.AddVariable("Line3", false);
                //_research.AddVariable("Line4", false);
                //_research.AddVariable("Line5", false);
                //_research.AddVariable("Line6", false);
                //_research.AddVariable("Line7", false);
                //_research.AddVariable("Line8", false);

                _settings.AddVariable("Merge", false);

                _settings["Toggle"] = false;

                SettingsController.RegisterSettingsWindow("InfBuddy", pluginDir + "\\UI\\InfBuddySettingWindow.xml", _settings);
                //SettingsController.RegisterSettingsWindow("Research", pluginDir + $"\\UI\\InfBuddyResearch{DynelManager.LocalPlayer.Profession}View.xml", _research);

                Chat.RegisterCommand("buddy", InfBuddyCommand);

                _stateMachine = new StateMachine(new IdleState());

                NavMeshMovementController = new NavMeshMovementController($"{pluginDir}\\NavMeshes", true);
                MovementController.Set(NavMeshMovementController);

                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Stop, OnStopMessage);

                Chat.RegisterCommand("delete", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    foreach (Mission mission in Mission.List)
                    {
                        if (mission.DisplayName.Contains("The Purification"))
                            mission.Delete();
                    }
                });

                NpcDialog.AnswerListChanged += NpcDialog_AnswerListChanged;
                Team.TeamRequest += OnTeamRequest;
                Game.OnUpdate += OnUpdate;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        //public static void SetResearchLine(int line)
        //{
        //    List<PerkLine> _perkLines = _researchMapHashs[DynelManager.LocalPlayer.Profession].Keys.ToList();

        //    DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchGoal,
        //        GetResearchHash(DynelManager.LocalPlayer.Profession, _perkLines[line - 1],
        //            Perk.GetPerkLineLevel(_perkLines[line - 1])));
        //}

        //public static void InitAddResearch()
        //{
        //    if (_research["Line1"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(1))
        //            _researchLinesActive.Add(1);
        //    }
        //    if (_research["Line2"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(2))
        //            _researchLinesActive.Add(2);
        //    }
        //    if (_research["Line3"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(3))
        //            _researchLinesActive.Add(3);
        //    }
        //    if (_research["Line4"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(4))
        //            _researchLinesActive.Add(4);
        //    }
        //    if (_research["Line5"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(5))
        //            _researchLinesActive.Add(5);
        //    }
        //    if (_research["Line6"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(6))
        //            _researchLinesActive.Add(6);
        //    }
        //    if (_research["Line7"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(7))
        //            _researchLinesActive.Add(7);
        //    }
        //    if (_research["Line8"].AsBool())
        //    {
        //        if (!_researchLinesActive.Contains(8))
        //            _researchLinesActive.Add(8);
        //    }
        //}

        //public static void InitRemoveResearch()
        //{
        //    if (!_research["Line1"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(1))
        //            _researchLinesActive.Remove(1);
        //    }
        //    if (!_research["Line2"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(2))
        //            _researchLinesActive.Remove(2);
        //    }
        //    if (!_research["Line3"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(3))
        //            _researchLinesActive.Remove(3);
        //    }
        //    if (!_research["Line4"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(4))
        //            _researchLinesActive.Remove(4);
        //    }
        //    if (!_research["Line5"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(5))
        //            _researchLinesActive.Remove(5);
        //    }
        //    if (!_research["Line6"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(6))
        //            _researchLinesActive.Remove(6);
        //    }
        //    if (!_research["Line7"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(7))
        //            _researchLinesActive.Remove(7);
        //    }
        //    if (!_research["Line8"].AsBool())
        //    {
        //        if (_researchLinesActive.Contains(8))
        //            _researchLinesActive.Remove(8);
        //    }
        //}

        //public static int GetResearchHash(Profession profession, PerkLine perkLine, int researchLevel)
        //{
        //    if (researchLevel > 1)
        //    {
        //        return _researchMapHashs[profession][perkLine] + (researchLevel - 1);
        //    }

        //    return _researchMapHashs[profession][perkLine] + researchLevel;
        //}

        private void Start()
        {
            Toggle = true;

            if (!IsLeader && !(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            Toggle = false;

            _stateMachine.SetState(new IdleState());
            NavMeshMovementController.Halt();
        }

        public static void StartNextRound()
        {
            IPCChannel.Broadcast(new StartMessage()
            {
                MissionDifficulty = Config.CharSettings[Game.ClientInst].MissionDifficulty,
                MissionFaction = Config.CharSettings[Game.ClientInst].MissionFaction,
            });
        }

        //private void ResearchView(object s, ButtonBase button)
        //{
        //    researchWindow = Window.CreateFromXml("Research", PluginDirectory + $"\\UI\\InfBuddyResearch{DynelManager.LocalPlayer.Profession}View.xml",
        //        windowSize: new Rect(0, 0, 220, 220),
        //        windowStyle: WindowStyle.Default,
        //        windowFlags: WindowFlags.NoFade);

        //    researchWindow.Show(true);
        //}

        private void InfoView(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDirectory + "\\UI\\InfBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            infoWindow.Show(true);
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            StartMessage startMsg = (StartMessage)msg;

            Config.CharSettings[Game.ClientInst].MissionDifficulty = startMsg.MissionDifficulty;
            Config.CharSettings[Game.ClientInst].MissionFaction = startMsg.MissionFaction;

            SettingsController.InfBuddyDifficulty = startMsg.MissionDifficulty.ToString();
            SettingsController.InfBuddyFaction = startMsg.MissionFaction.ToString();

            Config.Save();

            if (!_settings["Toggle"].AsBool())
            {
                _settings["Toggle"] = true;
                Chat.WriteLine("InfBuddy enabled.");
            }

            Leader = new Identity(IdentityType.SimpleChar, sender);
            Start();
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            Chat.WriteLine("InfBuddy disabled.");
            Stop();
        }

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("FactionBox", out TextInputView textinput1);
                SettingsController.settingsWindow.FindView("DifficultyBox", out TextInputView textinput2);
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView textinput3);

                if (textinput1 != null && textinput1.Text != String.Empty)
                {
                    if (Enum.TryParse(textinput1.Text, true, out MissionFaction factionValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].MissionFaction != factionValue)
                        {
                            Config.CharSettings[Game.ClientInst].MissionFaction = factionValue;
                            SettingsController.InfBuddyFaction = textinput1.Text;
                            Config.Save();
                        }
                    }
                }

                if (textinput2 != null && textinput2.Text != String.Empty)
                {
                    if (Enum.TryParse(textinput2.Text, true, out MissionDifficulty difficultyValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].MissionDifficulty != difficultyValue)
                        {
                            Config.CharSettings[Game.ClientInst].MissionDifficulty = difficultyValue;
                            SettingsController.InfBuddyDifficulty = textinput2.Text;
                            Config.Save();
                        }
                    }
                }

                if (textinput3 != null && textinput3.Text != String.Empty)
                {
                    if (int.TryParse(textinput3.Text, out int channelValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        {
                            IPCChannel.SetChannelId(Convert.ToByte(channelValue));
                            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(channelValue);
                            SettingsController.InfBuddyChannel = channelValue.ToString();
                            Config.Save();
                        }
                    }
                }

                if (SettingsController.settingsWindow.FindView("InfBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = InfoView;
                }

                //if (SettingsController.settingsWindow.FindView("InfBuddyResearchView", out Button researchView))
                //{
                //    researchView.Tag = SettingsController.settingsWindow;
                //    researchView.Clicked = ResearchView;
                //}

                if (_settings["Leecher"].AsBool() && Config.CharSettings[Game.ClientInst].IsLeech != true)
                {
                    Config.CharSettings[Game.ClientInst].IsLeech = true;
                    Config.Save();
                    return;
                }
                if (!_settings["Leecher"].AsBool() && Config.CharSettings[Game.ClientInst].IsLeech == true)
                {
                    Config.CharSettings[Game.ClientInst].IsLeech = false;
                    Config.Save();
                    return;
                }

                if (!_settings["Toggle"].AsBool() && Toggle == true)
                {
                    Stop();
                    IPCChannel.Broadcast(new StopMessage());
                    return;
                }
                if (_settings["Toggle"].AsBool() && Toggle == false)
                {
                    if (!InfBuddy._settings["Merge"].AsBool())
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;
                    }

                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        IPCChannel.Broadcast(new StartMessage()
                        {
                            MissionDifficulty = Config.CharSettings[Game.ClientInst].MissionDifficulty,
                            MissionFaction = Config.CharSettings[Game.ClientInst].MissionFaction,
                        });
                    }
                    Start();

                    return;
                }

                if (_settings["DoubleReward"].AsBool() && Config.CharSettings[Game.ClientInst].DoubleReward != true)
                {
                    Config.CharSettings[Game.ClientInst].DoubleReward = true;
                    Config.Save();
                    return;
                }
                if (!_settings["DoubleReward"].AsBool() && Config.CharSettings[Game.ClientInst].DoubleReward == true)
                {
                    Config.CharSettings[Game.ClientInst].DoubleReward = false;
                    Config.Save();
                    return;
                }

                if (_settings["Pathing"].AsBool() && Config.CharSettings[Game.ClientInst].PathToMob != true)
                {
                    Config.CharSettings[Game.ClientInst].PathToMob = true;
                    Config.Save();
                    return;
                }
                if (!_settings["Pathing"].AsBool() && Config.CharSettings[Game.ClientInst].PathToMob == true)
                {
                    Config.CharSettings[Game.ClientInst].PathToMob = false;
                    Config.Save();
                    return;
                }
            }


            if (SettingsController.InfBuddyDifficulty == String.Empty)
            {
                SettingsController.InfBuddyDifficulty = Config.MissionDifficulty.ToString();
            }

            if (SettingsController.InfBuddyFaction == String.Empty)
            {
                SettingsController.InfBuddyFaction = Config.MissionFaction.ToString();
            }

            if (SettingsController.InfBuddyChannel == String.Empty)
            {
                SettingsController.InfBuddyChannel = Config.IPCChannel.ToString();
            }

            _stateMachine.Tick();
        }
        public static SimpleChar GetLeader()
        {
            if (InfBuddy.Leader != Identity.None)
            {
                SimpleChar Leader = DynelManager.Players
                    .Where(c => c.Identity == InfBuddy.Leader)
                    .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) < 45f)
                    .Where(c => c.IsValid)
                    .FirstOrDefault();

                if (Leader != null) { return Leader; }
                else
                    return DynelManager.LocalPlayer;
            }
            else
            {
                TeamMember Lead = Team.Members
                    .Where(c => c.IsLeader)
                    .FirstOrDefault(c => c != null);

                SimpleChar Leader = DynelManager.Players
                    .Where(c => c.Identity == Lead.Identity)
                    .Where(c => c.DistanceFrom(DynelManager.LocalPlayer) < 45f)
                    .Where(c => c.IsValid)
                    .FirstOrDefault();

                if (Leader != null) { return Leader; }
                else
                    return DynelManager.LocalPlayer;
            }
        }

        private void OnTeamRequest(object s, TeamRequestEventArgs e)
        {
            if (e.Requester != Leader)
            {
                if (Toggle)
                    e.Ignore();

                return;
            }

            e.Accept();
        }

        private void NpcDialog_AnswerListChanged(object s, Dictionary<int, string> options)
        {
            SimpleChar dialogNpc = DynelManager.GetDynel((Identity)s).Cast<SimpleChar>();

            if (dialogNpc.Name == Constants.QuestGiverName)
            {
                foreach (KeyValuePair<int, string> option in options)
                {
                    if (option.Value == "Is there anything I can help you with?" ||
                        (Config.MissionFaction == MissionFaction.Clan && option.Value == "I will defend against the Unredeemed!") ||
                        (Config.MissionFaction == MissionFaction.Omni && option.Value == "I will defend against the Redeemed!") ||
                        (Config.MissionFaction == MissionFaction.Neutral && option.Value == "I will defend against the creatures of the brink!") ||
                        (Config.MissionDifficulty == MissionDifficulty.Easy && !Config.DoubleReward && option.Value == "I will deal with only the weakest aversaries") || //Brink missions have a typo
                        (Config.MissionDifficulty == MissionDifficulty.Easy && !Config.DoubleReward && option.Value == "I will deal with only the weakest adversaries") ||
                        (Config.MissionDifficulty == MissionDifficulty.Medium && !Config.DoubleReward && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                        (Config.MissionDifficulty == MissionDifficulty.Hard && !Config.DoubleReward && option.Value == "I will purge the temple of any and all assailants") ||
                        (Config.MissionDifficulty == MissionDifficulty.Hard && Config.DoubleReward && Trigger == false && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                        (Config.MissionDifficulty == MissionDifficulty.Hard && Config.DoubleReward && Trigger == true && option.Value == "I will purge the temple of any and all assailants")
                        )
                        NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                }
            }
            else if (dialogNpc.Name == Constants.QuestStarterName)
            {
                foreach (KeyValuePair<int, string> option in options)
                {
                    if (option.Value == "Yes, I am ready.")
                        NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                }
            }
        }

        private void InfBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Toggle"].AsBool() && !Toggle)
                    {
                        if (!InfBuddy._settings["Merge"].AsBool())
                        {
                            IsLeader = true;
                            Leader = DynelManager.LocalPlayer.Identity;
                        }

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StartMessage());
                        }
                        _settings["Toggle"] = true;

                        Start();
                        Chat.WriteLine("Starting");
                    }
                    else if (_settings["Toggle"].AsBool() && Toggle)
                    {
                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StopMessage());
                        }
                        Stop();
                        Chat.WriteLine("Stopping");
                    }
                    return;
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
