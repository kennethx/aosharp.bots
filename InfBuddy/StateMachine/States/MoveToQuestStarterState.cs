﻿using AOSharp.Core;
using AOSharp.Core.UI;

namespace InfBuddy
{
    public class MoveToQuestStarterState : IState
    {
        private static bool Switch = false;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (!InfBuddy.NavMeshMovementController.IsNavigating && IsAtQuestStarter())
            {
                if (InfBuddy.IsLeader)
                    return new StartMissionState();
                else if (!InfBuddy.IsLeader)
                    return new DefendSpiritState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy._stateTimeOut = Time.NormalTime;

            //Chat.WriteLine("MoveToQuestStarter::OnStateEnter");

            if (!InfBuddy.NavMeshMovementController.IsNavigating && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestStarterBeforePos) > 4f)
            {
                InfBuddy.NavMeshMovementController.SetDestination(Constants.QuestStarterBeforePos);
                Switch = true;
            }
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("MoveToQuestStarter::OnStateExit");
            Switch = false;
        }

        public void Tick()
        {
            if (!InfBuddy.NavMeshMovementController.IsNavigating && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestStarterBeforePos) < 30f && Switch == true)
            {
                InfBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.QuestStarterPos);
            }
        }

        private bool IsAtQuestStarter()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestStarterPos) < 8f;
        }
    }
}
