﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using AttackBuddy.IPCMessages;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;
using AOSharp.Common.GameData.UI;
using System.Globalization;

namespace AttackBuddy
{
    public class AttackBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static bool Toggle = false;

        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public static double _stateTimeOut = Time.NormalTime;

        public static List<SimpleChar> _mob = new List<SimpleChar>();
        public static List<SimpleChar> _bossMob = new List<SimpleChar>();
        public static List<SimpleChar> _switchMob = new List<SimpleChar>();

        public static List<SimpleChar> _switchMobPrecision = new List<SimpleChar>();
        public static List<SimpleChar> _switchMobCharging = new List<SimpleChar>();
        public static List<SimpleChar> _switchMobShield = new List<SimpleChar>();

        private static double _refreshList;

        public static string PluginDirectory;

        public static Settings AttackBuddySettings = new Settings("AttackBuddy");

        public static List<string> OSHelpers = new List<string>();

        public static List<Vector3> RoamingVectors = new List<Vector3>();

        public static List<string> NamesToIgnore = new List<string>
        {
                    "Alien Fuel Tower",
                    "Kyr'Ozch Guardian",
                    "Engorged Sacrificial Husks",
                    "Honor Guard - Ilari'Uri",
                    "Representative of IPS",
                    "Transportation Officer Darren Plush",
                    "Unicorn Field Engineer",
                    "Buckethead Technodealer",
                    "Unicorn Commander Labbe",
                    "Professor Raji Masoud",
                    "Slimy Alien Plant",
                    "Awakened Xan",
                    "Altar of Torture",
                    "Altar of Purification",
                    "Calan-Cur",
                    "Spirit of Judgement",
                    "Wandering Spirit",
                    "Altar of Torture",
                    "Altar of Purification",
                    "Unicorn Coordinator Magnum Blaine",
                    "Xan Spirit",
                    "Watchful Spirit",
                    "Amesha Vizaresh",
                    "Guardian Spirit of Purification",
                    "Tibor 'Rocketman' Nagy",
                    "One Who Obeys Precepts",
                    "The Retainer Of Ergo",
                    "Outzone Supplier",
                    "Hollow Island Weed",
                    "Sheila Marlene",
                    "Unicorn Advance Sentry",
                    "Unicorn Technician",
                    "Basic Tools Merchant",
                    "Container Supplier",
                    "Basic Quality Pharmacist",
                    "Basic Quality Armorer",
                    "Basic Quality Weaponsdealer",
                    "Tailor",
                    "Unicorn Commander Rufus",
                    "Ergo, Inferno Guardian of Shadows",
                    "Unicorn Trooper",
                    "Unicorn Squadleader",
                    "Rookie Alien Hunter",
                    "Unicorn Service Tower Alpha",
                    "Unicorn Service Tower Delta",
                    "Unicorn Service Tower Gamma",
                    "Sean Powell",
                    "Xan Spirit",
                    "Unicorn Guard",
                    "Essence Fragment",
                    "Scalding Flames",
                    "Guide",
                    "Searing Flame",
                    "Searing Flames",
                    "Guard",
                    "Awakened Xan",
                    "Fourth Watch Down",
                    "Mortar Bombardment",
                    "Dogmatic Pestilence",
                    "Flame of the Immortal One",
                    "Calan-Cur",
                    "Pulsating Ice Obelisk",
                    "Unnatural Ice",
                    "Hacked Mechdog",
                    "Harbinger of Pestilence"
        };

        public override void Run(string pluginDir)
        {
            try
            {

                PluginDirectory = pluginDir;

                Chat.WriteLine("AttackBuddy Loaded!");
                Chat.WriteLine("/attackbuddy for settings.");

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy\\{Game.ClientInst}\\Config.json");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                AttackBuddySettings.AddVariable("Running", false);

                AttackBuddySettings.AddVariable("Attack", false);
                AttackBuddySettings.AddVariable("Defend", false);
                AttackBuddySettings.AddVariable("Roam", false);
                AttackBuddySettings.AddVariable("Nuke", false);

                AttackBuddySettings.AddVariable("AggroTool", false);
                AttackBuddySettings.AddVariable("LootDelay", false);

                AttackBuddySettings["Running"] = false;
                AttackBuddySettings["Attack"] = false;
                AttackBuddySettings["Defend"] = false;
                AttackBuddySettings["Roam"] = false;
                AttackBuddySettings["Nuke"] = false;

                Chat.RegisterCommand("buddy", AttackBuddyCommand);

                SettingsController.RegisterSettingsWindow("AttackBuddy", pluginDir + "\\UI\\AttackBuddySettingWindow.xml", AttackBuddySettings);

                _stateMachine = new StateMachine(new IdleState());

                IPCChannel.RegisterCallback((int)IPCOpcode.StartMode, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopMode, OnStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.SetPos, OnSetPosMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.SetResetPos, OnSetResetPosMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.NoneSelected, OnNoneSelectedMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.ThreatRange, OnThreatRangeMessage);

                Game.OnUpdate += OnUpdate;
                //DynelManager.DynelSpawned += OnDynelSpawned;
                //DynelManager.DynelDespawned += OnDynelSpawned;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void OnSetPosMessage(int sender, IPCMessage msg)
        {
            SetPosMessage setposMsg = (SetPosMessage)msg;

            Chat.WriteLine("Position set.");

            AttackBuddySettings["Defend"] = true;

            Constants._posToDefend = DynelManager.LocalPlayer.Position;
        }

        private void OnSetResetPosMessage(int sender, IPCMessage msg)
        {
            SetResetPosMessage setresetposMsg = (SetResetPosMessage)msg;

            Chat.WriteLine("Position reset.");

            AttackBuddySettings["Defend"] = false;

            Constants._posToDefend = Vector3.Zero;
        }

        private void OnNoneSelectedMessage(int sender, IPCMessage msg)
        {
            NoneSelectedMessage noneMsg = (NoneSelectedMessage)msg;

            Chat.WriteLine("No mode selected.");
        }

        private void OnThreatRangeMessage(int sender, IPCMessage msg)
        {
            ThreatRangeMessage rangeMsg = (ThreatRangeMessage)msg;

            Config.CharSettings[Game.ClientInst].ThreatRange = rangeMsg.Range;
            SettingsController.AttackBuddyThreatRange = rangeMsg.Range;
        }

        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (DynelManager.LocalPlayer.Identity == Leader)
                return;

            Toggle = true;

            StartModeMessage startMsg = (StartModeMessage)msg;

            AttackBuddySettings["Attack"] = startMsg.Attack;
            AttackBuddySettings["Defend"] = startMsg.Defend;
            AttackBuddySettings["Roam"] = startMsg.Roam;
            AttackBuddySettings["Nuke"] = startMsg.Nuke;

            AttackBuddySettings["Running"] = true;


            Leader = new Identity(IdentityType.SimpleChar, sender);

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            StopModeMessage stopMsg = (StopModeMessage)msg;

            if (!AttackBuddySettings["Attack"].AsBool() && !AttackBuddySettings["Defend"].AsBool()
                && !AttackBuddySettings["Nuke"].AsBool() && !AttackBuddySettings["Roam"].AsBool() 
                && AttackBuddySettings["Running"].AsBool() && Toggle == true)
            {
                Chat.WriteLine("No mode selected.");
            }

            Toggle = false;

            AttackBuddySettings["Attack"] = stopMsg.Attack;
            AttackBuddySettings["Defend"] = stopMsg.Defend;
            AttackBuddySettings["Nuke"] = stopMsg.Nuke;
            AttackBuddySettings["Roam"] = stopMsg.Roam;

            AttackBuddySettings["Running"] = false;

            _stateMachine.SetState(new IdleState());

            RoamState.FirstWaypoint = false;
            RoamState._counter = 0;

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();
        }

        private void Start()
        {
            Toggle = true;

            if (!(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void Stop()
        {
            if (!AttackBuddySettings["Attack"].AsBool() && !AttackBuddySettings["Defend"].AsBool()
                && !AttackBuddySettings["Nuke"].AsBool() && !AttackBuddySettings["Roam"].AsBool()
                && AttackBuddySettings["Running"].AsBool() && Toggle == true)
            {
                Chat.WriteLine("No mode selected.");
            }

            Toggle = false;

            AttackBuddySettings["Running"] = false;

            _stateMachine.SetState(new IdleState());

            RoamState.FirstWaypoint = false;
            RoamState._counter = 0;

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
            if (MovementController.Instance.IsNavigating)
                MovementController.Instance.Halt();
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        private void AddWaypoint(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                RoamingVectors.Add(DynelManager.LocalPlayer.Position);
            }
        }

        private void ClearWaypoints(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                RoamingVectors.Clear();
            }
        }

        private void AddHelper(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("AddHelperBox", out TextInputView textinput);

                Task.Factory.StartNew(
                     async () =>
                     {
                         await Task.Delay(200);

                         if (textinput.Text != String.Empty)
                         {
                             OSHelpers.Add($"{textinput.Text}");
                         }
                     });
            }
        }

        private void ClearHelpers(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                OSHelpers.Clear();
            }
        }

        private void PrintHelpers(object s, ButtonBase button)
        {
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                if (OSHelpers.Count == 0)
                {
                    Chat.WriteLine("Empty.");
                    return;
                }

                foreach (string helper in OSHelpers)
                {
                    Chat.WriteLine($"{helper}");
                }    
            }
        }

        private void HelpBox(object s, ButtonBase button)
        {
            Window helpWindow = Window.CreateFromXml("Help", PluginDirectory + "\\UI\\AttackBuddyHelpBox.xml",
            windowSize: new Rect(0, 0, 455, 345),
            windowStyle: WindowStyle.Default,
            windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);
            helpWindow.Show(true);
        }

        public static bool BossHasCorrespondingBuff(int buff)
        {
            SimpleChar boss = DynelManager.NPCs
                                .Where(c => c.Name == "Kyr'Ozch Technician" && c.Buffs.Contains(buff))
                                .FirstOrDefault();
            if (boss != null)
                return true;
            else
                return false;
        }

        private bool IsFightingAny(SimpleChar target)
        {
            if (Team.IsInTeam)
            {
                if (target.FightingTarget == null) { return true; }

                if (target.FightingTarget != null && target.FightingTarget == DynelManager.LocalPlayer) { return true; }

                if (target.FightingTarget != null && (Team.Members.Where(c => c.Name == target.FightingTarget.Name).Any()
                    || AttackBuddy.OSHelpers.Where(c => target.FightingTarget.Name == c).Any())) { return true; }

                if (target.FightingTarget != null && target.FightingTarget.IsPet
                    && Team.Members.Where(c => c.Name == target.FightingTarget.Name).Any()) { return true; }

                if (target.FightingTarget != null && (target.FightingTarget.Name == "Guardian Spirit of Purification"
                    || target.FightingTarget.Name == "Rookie Alien Hunter")) { return true; }

                return false;
            }
            else
            {
                if (target.FightingTarget == null) { return true; }

                if (target.FightingTarget != null && target.FightingTarget == DynelManager.LocalPlayer) { return true; }

                if (target.FightingTarget != null && (target.FightingTarget.Name == DynelManager.LocalPlayer.Name
                    || AttackBuddy.OSHelpers.Where(c => target.FightingTarget.Name == c).Any())) { return true; }

                if (target.FightingTarget != null &&
                    DynelManager.LocalPlayer.Pets.Where(c => target.FightingTarget.Name == c.Character.Name).Any()) { return true; }

                if (target.FightingTarget != null && (target.FightingTarget.Name == "Guardian Spirit of Purification"
                    || target.FightingTarget.Name == "Rookie Alien Hunter")) { return true; }

                return false;
            }
        }

        //private void OnDynelDespawned(object _, Dynel dynel)
        //{
        //    if (Playfield.ModelIdentity.Instance == 6123)
        //    {
        //        _switchMob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.Name != "Alien Cocoon" && c.Name != "Zix"
        //                && c.IsAlive && c.IsInLineOfSight && c.Name != "Kyr'Ozch Maid"
        //                && c.Name != "Kyr'Ozch Technician" && c.Name != "Defense Drone Tower"
        //                && c.Name != "Control Leech"
        //                && c.Name != "Alien Precision Tower" && c.Name != "Alien Charging Tower"
        //                && c.Name != "Alien Shield Tower" && c.Name != "Kyr'Ozch Technician"
        //                && c.MaxHealth < 1000000 && IsFightingAny(c))
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .OrderBy(c => c.HealthPercent)
        //            .OrderByDescending(c => c.Name == "Kyr'Ozch Nurse")
        //            .OrderByDescending(c => c.Name == "Kyr'Ozch Offspring")
        //            .OrderByDescending(c => c.Name == "Rimah Corsuezo")
        //            .ToList();

        //        _switchMobPrecision = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && BossHasCorrespondingBuff(287525) && c.Name == "Alien Precision Tower")
        //            .ToList();

        //        _switchMobCharging = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && BossHasCorrespondingBuff(287515) && c.Name == "Alien Charging Tower")
        //            .ToList();

        //        _switchMobShield = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && BossHasCorrespondingBuff(287526) && c.Name == "Alien Shield Tower")
        //            .ToList();

        //    }
        //    else
        //    {
        //        _bossMob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.IsAlive && c.IsInLineOfSight
        //                && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
        //                && !c.Buffs.Contains(NanoLine.ShovelBuffs)
        //                && c.MaxHealth >= 1000000)
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .ToList();

        //        _switchMob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.Name != "Zix" && !c.Name.Contains("sapling")
        //                && c.IsAlive && c.IsInLineOfSight && c.MaxHealth < 1000000
        //                && IsFightingAny(c) && (c.Name == "Devoted Fanatic" || c.Name == "Hallowed Acolyte" || c.Name == "Hand of the Colonel"
        //             || c.Name == "Hacker'Uri" || c.Name == "The Sacrifice" || c.Name == "Corrupted Xan-Len"
        //              || c.Name == "Blue Tower" || c.Name == "Green Tower" || c.Name == "Drone Harvester - Jaax'Sinuh"
        //               || c.Name == "Support Sentry - Ilari'Uri" || c.Name == "Fanatic" || c.Name == "Alien Cocoon"))
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .OrderBy(c => c.HealthPercent)
        //            .OrderByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
        //            .OrderByDescending(c => c.Name == "Alien Cocoon")
        //            .OrderByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
        //            .ToList();

        //        _mob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.Name != "Zix" && !c.Name.Contains("sapling") && c.IsAlive
        //                && c.IsInLineOfSight && c.MaxHealth < 1000000 && IsFightingAny(c))
        //                //&& c.Name != "Devoted Fanatic" && c.Name != "Hallowed Acolyte" && c.Name != "Hand of the Colonel"
        //                //&& c.Name != "Hacker'Uri" && c.Name != "The Sacrifice" && c.Name != "Corrupted Xan-Len"
        //                //&& c.Name != "Blue Tower" && c.Name != "Green Tower" && c.Name != "Drone Harvester - Jaax'Sinuh"
        //                //&& c.Name != "Support Sentry - Ilari'Uri" && c.Name != "Fanatic" && c.Name != "Alien Cocoon"
        //                //&& c.Name != "Kyr'Ozch Technician")
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .OrderBy(c => c.HealthPercent)
        //            .ToList();
        //    }
        //}

        //private void OnDynelSpawned(object _, Dynel dynel)
        //{
        //    if (Playfield.ModelIdentity.Instance == 6123)
        //    {
        //        _switchMob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.Name != "Alien Cocoon" && c.Name != "Zix"
        //                && c.IsAlive && c.IsInLineOfSight && c.Name != "Kyr'Ozch Maid"
        //                && c.Name != "Kyr'Ozch Technician" && c.Name != "Defense Drone Tower"
        //                && c.Name != "Control Leech"
        //                && c.Name != "Alien Precision Tower" && c.Name != "Alien Charging Tower"
        //                && c.Name != "Alien Shield Tower" && c.Name != "Kyr'Ozch Technician"
        //                && c.MaxHealth < 1000000 && IsFightingAny(c))
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .OrderBy(c => c.HealthPercent)
        //            .OrderByDescending(c => c.Name == "Kyr'Ozch Nurse")
        //            .OrderByDescending(c => c.Name == "Kyr'Ozch Offspring")
        //            .OrderByDescending(c => c.Name == "Rimah Corsuezo")
        //            .ToList();

        //        _switchMobPrecision = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && BossHasCorrespondingBuff(287525) && c.Name == "Alien Precision Tower")
        //            .ToList();

        //        _switchMobCharging = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && BossHasCorrespondingBuff(287515) && c.Name == "Alien Charging Tower")
        //            .ToList();

        //        _switchMobShield = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && BossHasCorrespondingBuff(287526) && c.Name == "Alien Shield Tower")
        //            .ToList();

        //    }
        //    else
        //    {
        //        _bossMob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.IsAlive && c.IsInLineOfSight
        //                && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
        //                && !c.Buffs.Contains(NanoLine.ShovelBuffs)
        //                && c.MaxHealth >= 1000000)
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .ToList();

        //        _switchMob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.Name != "Zix" && !c.Name.Contains("sapling")
        //                && c.IsAlive && c.IsInLineOfSight && c.MaxHealth < 1000000
        //                && IsFightingAny(c) && (c.Name == "Devoted Fanatic" || c.Name == "Hallowed Acolyte" || c.Name == "Hand of the Colonel"
        //             || c.Name == "Hacker'Uri" || c.Name == "The Sacrifice" || c.Name == "Corrupted Xan-Len"
        //              || c.Name == "Blue Tower" || c.Name == "Green Tower" || c.Name == "Drone Harvester - Jaax'Sinuh"
        //               || c.Name == "Support Sentry - Ilari'Uri" || c.Name == "Fanatic" || c.Name == "Alien Cocoon"))
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .OrderBy(c => c.HealthPercent)
        //            .OrderByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
        //            .OrderByDescending(c => c.Name == "Alien Cocoon")
        //            .OrderByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
        //            .ToList();

        //        _mob = DynelManager.NPCs
        //            .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
        //                && !AttackBuddy.NamesToIgnore.Contains(c.Name)
        //                && c.Name != "Zix" && !c.Name.Contains("sapling") && c.IsAlive
        //                && c.IsInLineOfSight && c.MaxHealth < 1000000 && IsFightingAny(c))
        //                //&& c.Name != "Devoted Fanatic" && c.Name != "Hallowed Acolyte" && c.Name != "Hand of the Colonel"
        //                //&& c.Name != "Hacker'Uri" && c.Name != "The Sacrifice" && c.Name != "Corrupted Xan-Len"
        //                //&& c.Name != "Blue Tower" && c.Name != "Green Tower" && c.Name != "Drone Harvester - Jaax'Sinuh"
        //                //&& c.Name != "Support Sentry - Ilari'Uri" && c.Name != "Fanatic" && c.Name != "Alien Cocoon"
        //                //&& c.Name != "Kyr'Ozch Technician")
        //            .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
        //            .OrderBy(c => c.HealthPercent)
        //            .ToList();
        //    }
        //}

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            if (Time.NormalTime - _refreshList >= 0.5
                && Toggle == true)
            {
                if (Playfield.ModelIdentity.Instance == 6123)
                {
                    _switchMob = DynelManager.NPCs
                        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                            && !AttackBuddy.NamesToIgnore.Contains(c.Name)
                            && c.Name != "Alien Cocoon" && c.Name != "Zix"
                            && c.IsAlive && c.IsInLineOfSight && c.Name != "Kyr'Ozch Maid"
                            && c.Name != "Kyr'Ozch Technician" && c.Name != "Defense Drone Tower"
                            && c.Name != "Control Leech"
                            && c.Name != "Alien Precision Tower" && c.Name != "Alien Charging Tower"
                            && c.Name != "Alien Shield Tower" && c.Name != "Kyr'Ozch Technician"
                            && c.MaxHealth < 1000000 && IsFightingAny(c))
                        .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                        .OrderBy(c => c.HealthPercent)
                        .OrderByDescending(c => c.Name == "Kyr'Ozch Nurse")
                        .OrderByDescending(c => c.Name == "Kyr'Ozch Offspring")
                        .OrderByDescending(c => c.Name == "Rimah Corsuezo")
                        .ToList();

                    _switchMobPrecision = DynelManager.NPCs
                        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                            && BossHasCorrespondingBuff(287525) && c.Name == "Alien Precision Tower")
                        .ToList();

                    _switchMobCharging = DynelManager.NPCs
                        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                            && BossHasCorrespondingBuff(287515) && c.Name == "Alien Charging Tower")
                        .ToList();

                    _switchMobShield = DynelManager.NPCs
                        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                            && BossHasCorrespondingBuff(287526) && c.Name == "Alien Shield Tower")
                        .ToList();

                }
                else
                {
                    _bossMob = DynelManager.NPCs
                        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                            && !AttackBuddy.NamesToIgnore.Contains(c.Name)
                            && c.IsAlive && c.IsInLineOfSight
                            && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
                            && !c.Buffs.Contains(NanoLine.ShovelBuffs)
                            && c.MaxHealth >= 1000000)
                        .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                        .ToList();

                    _switchMob = DynelManager.NPCs
                        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                            && !AttackBuddy.NamesToIgnore.Contains(c.Name)
                            && c.Name != "Zix" && !c.Name.Contains("sapling")
                            && c.IsAlive && c.IsInLineOfSight && c.MaxHealth < 1000000
                            && IsFightingAny(c) && (c.Name == "Director - Public Relations" || c.Name == "Stim Fiend" || c.Name == "Devoted Fanatic" || c.Name == "Hallowed Acolyte" || c.Name == "Hand of the Colonel"
                         || c.Name == "Hacker'Uri" || c.Name == "The Sacrifice" || c.Name == "Corrupted Xan-Len"
                          || c.Name == "Blue Tower" || c.Name == "Green Tower" || c.Name == "Drone Harvester - Jaax'Sinuh"
                           || c.Name == "Support Sentry - Ilari'Uri" || c.Name == "Fanatic" || c.Name == "Alien Cocoon"))
                        .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                        .OrderBy(c => c.HealthPercent)
                        .OrderByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
                        .OrderByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
                        .OrderByDescending(c => c.Name == "Alien Cocoon")
                        .ToList();

                    _mob = DynelManager.NPCs
                        .Where(c => c.DistanceFrom(AttackBuddy.GetLeader()) <= SettingsController.AttackBuddyThreatRange
                            && !AttackBuddy.NamesToIgnore.Contains(c.Name)
                            && c.Name != "Zix" && !c.Name.Contains("sapling") && c.IsAlive
                            && c.IsInLineOfSight && c.MaxHealth < 1000000 && IsFightingAny(c))
                            //&& c.Name != "Devoted Fanatic" && c.Name != "Hallowed Acolyte" && c.Name != "Hand of the Colonel"
                            //&& c.Name != "Hacker'Uri" && c.Name != "The Sacrifice" && c.Name != "Corrupted Xan-Len"
                            //&& c.Name != "Blue Tower" && c.Name != "Green Tower" && c.Name != "Drone Harvester - Jaax'Sinuh"
                            //&& c.Name != "Support Sentry - Ilari'Uri" && c.Name != "Fanatic" && c.Name != "Alien Cocoon"
                            //&& c.Name != "Kyr'Ozch Technician")
                        .OrderBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                        .OrderBy(c => c.HealthPercent)
                        //.OrderBy(c => c.HealthPercent)
                        //.ThenBy(c => c.Position.DistanceFrom(AttackBuddy.GetLeader().Position))
                        .OrderByDescending(c => c.Name == "Corrupted Hiisi Berserker")
                        .OrderByDescending(c => c.Name == "Corrupted Xan-Cur")
                        .OrderByDescending(c => c.Name == "Corrupted Xan-Kuir")
                        .OrderByDescending(c => c.Name == "Alien Cocoon")
                        .OrderByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
                        .OrderByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
                        .OrderByDescending(c => c.Name == "Stim Fiend")
                        .OrderByDescending(c => c.Name == "Masked Operator")
                        .OrderByDescending(c => c.Name == "Masked Technician")
                        .OrderByDescending(c => c.Name == "Masked Engineer")
                        .OrderByDescending(c => c.Name == "Masked Superior Commando")
                        .OrderByDescending(c => c.Name == "Green Tower")
                        .OrderByDescending(c => c.Name == "Blue Tower")
                        .OrderByDescending(c => c.Name == "The Sacrifice")
                        .OrderByDescending(c => c.Name == "Hacker'Uri")
                        .OrderByDescending(c => c.Name == "Hand of the Colonel")
                        .OrderByDescending(c => c.Name == "Corrupted Xan-Len")
                        .OrderByDescending(c => c.Name == "Hallowed Acolyte")
                        .OrderByDescending(c => c.Name == "Devoted Fanatic")
                        .ToList();
                }

                _refreshList = Time.NormalTime;
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView textinput1);
                SettingsController.settingsWindow.FindView("ThreatRangeBox", out TextInputView textinput2);

                if (textinput1 != null && textinput1.Text != String.Empty)
                {
                    if (int.TryParse(textinput1.Text, out int channelValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        {
                            IPCChannel.SetChannelId(Convert.ToByte(channelValue));
                            Config.CharSettings[Game.ClientInst].IPCChannel = Convert.ToByte(channelValue);
                            SettingsController.AttackBuddyChannel = channelValue.ToString();
                            Config.Save();
                        }
                    }
                }

                if (textinput2 != null && textinput2.Text != String.Empty)
                {
                    if (int.TryParse(textinput2.Text, out int rangeValue))
                    {
                        if (Config.CharSettings[Game.ClientInst].ThreatRange != rangeValue)
                        {
                            IPCChannel.Broadcast(new ThreatRangeMessage()
                            {
                                Range = rangeValue
                            });
                            Config.CharSettings[Game.ClientInst].ThreatRange = rangeValue;
                            SettingsController.AttackBuddyThreatRange = rangeValue;
                            Config.Save();
                        }
                    }
                }

                if (SettingsController.settingsView != null)
                {
                    if (SettingsController.settingsView.FindChild("AttackBuddyHelpBox", out Button helpBox))
                    {
                        helpBox.Tag = SettingsController.settingsView;
                        helpBox.Clicked = HelpBox;
                    }

                    if (SettingsController.settingsView.FindChild("AddWaypoint", out Button addWaypoint))
                    {
                        addWaypoint.Tag = SettingsController.settingsView;
                        addWaypoint.Clicked = AddWaypoint;
                    }

                    if (SettingsController.settingsView.FindChild("ClearWaypoints", out Button clearWaypoints))
                    {
                        clearWaypoints.Tag = SettingsController.settingsView;
                        clearWaypoints.Clicked = ClearWaypoints;
                    }

                    if (SettingsController.settingsView.FindChild("AddHelper", out Button addHelper))
                    {
                        addHelper.Tag = SettingsController.settingsView;
                        addHelper.Clicked = AddHelper;
                    }

                    if (SettingsController.settingsView.FindChild("ClearHelpers", out Button clearHelpers))
                    {
                        clearHelpers.Tag = SettingsController.settingsView;
                        clearHelpers.Clicked = ClearHelpers;
                    }

                    if (SettingsController.settingsView.FindChild("PrintHelpers", out Button printHelpers))
                    {
                        printHelpers.Tag = SettingsController.settingsView;
                        printHelpers.Clicked = PrintHelpers;
                    }
                }

                if (!AttackBuddySettings["Attack"].AsBool() && !AttackBuddySettings["Defend"].AsBool()
                    && !AttackBuddySettings["Nuke"].AsBool() && !AttackBuddySettings["Roam"].AsBool()
                    && AttackBuddySettings["Running"].AsBool() && Toggle == true)
                {
                    Stop();
                    IPCChannel.Broadcast(new StopModeMessage());
                    return;
                }

                if (AttackBuddySettings["Defend"].AsBool() && Constants._posToDefend == Vector3.Zero)
                {
                    Constants._posToDefend = DynelManager.LocalPlayer.Position;
                    IPCChannel.Broadcast(new SetPosMessage());

                    Chat.WriteLine("Position set.");
                    return;
                }

                if (!AttackBuddySettings["Defend"].AsBool() && Constants._posToDefend != Vector3.Zero)
                {
                    Constants._posToDefend = Vector3.Zero;
                    IPCChannel.Broadcast(new SetResetPosMessage());

                    Chat.WriteLine("Position reset.");
                    return;
                }

                if (!AttackBuddySettings["Running"].AsBool() && Toggle)
                {
                    Stop();
                    IPCChannel.Broadcast(new StopModeMessage()
                    {
                        Attack = AttackBuddySettings["Attack"].AsBool(),
                        Defend = AttackBuddySettings["Defend"].AsBool(),
                        Roam = AttackBuddySettings["Roam"].AsBool(),
                        Nuke = AttackBuddySettings["Nuke"].AsBool()
                    });
                    return;
                }
                if (AttackBuddySettings["Running"].AsBool() && !Toggle)
                {
                    if (AttackBuddySettings["Attack"].AsBool() && AttackBuddySettings["Defend"].AsBool()
                         && AttackBuddySettings["Nuke"].AsBool() && AttackBuddySettings["Roam"].AsBool())
                    {
                        AttackBuddySettings["Attack"] = false;
                        AttackBuddySettings["Defend"] = false;
                        AttackBuddySettings["Nuke"] = false;
                        AttackBuddySettings["Roam"] = false;
                        AttackBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (AttackBuddySettings["Attack"].AsBool() && AttackBuddySettings["Defend"].AsBool()
                         && AttackBuddySettings["Roam"].AsBool())
                    {
                        AttackBuddySettings["Attack"] = false;
                        AttackBuddySettings["Defend"] = false;
                        AttackBuddySettings["Nuke"] = false;
                        AttackBuddySettings["Roam"] = false;
                        AttackBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (AttackBuddySettings["Attack"].AsBool() && AttackBuddySettings["Nuke"].AsBool()
                        && AttackBuddySettings["Roam"].AsBool())
                    {
                        AttackBuddySettings["Attack"] = false;
                        AttackBuddySettings["Defend"] = false;
                        AttackBuddySettings["Nuke"] = false;
                        AttackBuddySettings["Roam"] = false;
                        AttackBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (AttackBuddySettings["Defend"].AsBool() && AttackBuddySettings["Attack"].AsBool()
                        && AttackBuddySettings["Roam"].AsBool())
                    {
                        AttackBuddySettings["Attack"] = false;
                        AttackBuddySettings["Defend"] = false;
                        AttackBuddySettings["Nuke"] = false;
                        AttackBuddySettings["Roam"] = false;
                        AttackBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    if (AttackBuddySettings["Defend"].AsBool() && AttackBuddySettings["Nuke"].AsBool()
                        && AttackBuddySettings["Roam"].AsBool())
                    {
                        AttackBuddySettings["Attack"] = false;
                        AttackBuddySettings["Defend"] = false;
                        AttackBuddySettings["Nuke"] = false;
                        AttackBuddySettings["Roam"] = false;
                        AttackBuddySettings["Running"] = false;

                        Chat.WriteLine($"Can only toggle one mode.");
                        return;
                    }

                    IsLeader = true;
                    Leader = DynelManager.LocalPlayer.Identity;

                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        IPCChannel.Broadcast(new StartModeMessage()
                        {
                            Attack = AttackBuddySettings["Attack"].AsBool(),
                            Defend = AttackBuddySettings["Defend"].AsBool(),
                            Roam = AttackBuddySettings["Roam"].AsBool(),
                            Nuke = AttackBuddySettings["Nuke"].AsBool()
                        });
                    }
                    Start();

                    return;
                }

                if (AttackBuddySettings["AggroTool"].AsBool() && !Config.CharSettings[Game.ClientInst].AggroTool)
                {
                    Config.CharSettings[Game.ClientInst].AggroTool = true;
                    Config.Save();
                    return;
                }
                if (!AttackBuddySettings["AggroTool"].AsBool() && Config.CharSettings[Game.ClientInst].AggroTool)
                {
                    Config.CharSettings[Game.ClientInst].AggroTool = false;
                    Config.Save();
                    return;
                }

                if (AttackBuddySettings["LootDelay"].AsBool() && !Config.CharSettings[Game.ClientInst].LootDelay)
                {
                    Config.CharSettings[Game.ClientInst].LootDelay = true;
                    Config.Save();
                    return;
                }
                if (!AttackBuddySettings["LootDelay"].AsBool() && Config.CharSettings[Game.ClientInst].LootDelay)
                {
                    Config.CharSettings[Game.ClientInst].LootDelay = false;
                    Config.Save();
                    return;
                }

            }

            if (AttackBuddySettings["AggroTool"].AsBool())
            {
                Inventory.Find(83920, 83919, out Item aggroTool);
                Inventory.Find(83919, 83919, out Item aggroMultiTool);
                Inventory.Find(244655, 244655, out Item scorpioTool);

                if (aggroTool == null && aggroMultiTool == null && scorpioTool == null)
                {
                    Chat.WriteLine($"This requires an Aggression Tool.");
                    AttackBuddy.AttackBuddySettings["AggroTool"] = false;
                    AttackBuddy.Config.Save();
                }
            }

            if (RoamingVectors.Count >= 1 && AttackBuddySettings["Roam"].AsBool())
            {
                foreach (Vector3 pos in AttackBuddy.RoamingVectors)
                {
                    Debug.DrawSphere(pos, 0.2f, DebuggingColor.White);
                }
            }

            if (SettingsController.AttackBuddyChannel == String.Empty)
            {
                SettingsController.AttackBuddyChannel = Config.IPCChannel.ToString();
            }

            if (SettingsController.AttackBuddyThreatRange != Config.ThreatRange)
            {
                SettingsController.AttackBuddyThreatRange = Config.ThreatRange;
            }

            _stateMachine.Tick();

        }

        public static SimpleChar GetLeader()
        {
            if (AttackBuddy.Leader != Identity.None)
            {
                SimpleChar Leader = DynelManager.Players
                    .Where(c => c.Identity == AttackBuddy.Leader && c.DistanceFrom(DynelManager.LocalPlayer) < 45f
                        && c.IsValid && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301))
                    .FirstOrDefault();

                if (Leader != null) { return Leader; }
                else
                    return DynelManager.LocalPlayer;
            }

            return DynelManager.LocalPlayer;
        }

        private void AttackBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!AttackBuddySettings["Attack"].AsBool() && !AttackBuddySettings["Defend"].AsBool()
                        && !AttackBuddySettings["Nuke"].AsBool() && !AttackBuddySettings["Roam"].AsBool())
                    {
                        Chat.WriteLine("No mode selected.");
                        IPCChannel.Broadcast(new NoneSelectedMessage());
                        return;
                    }


                    if (!AttackBuddySettings["Running"].AsBool() && !Toggle)
                    {
                        IsLeader = true;
                        Leader = DynelManager.LocalPlayer.Identity;

                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            IPCChannel.Broadcast(new StartModeMessage()
                            {
                                Attack = AttackBuddySettings["Attack"].AsBool(),
                                Defend = AttackBuddySettings["Defend"].AsBool(),
                                Roam = AttackBuddySettings["Roam"].AsBool(),
                                Nuke = AttackBuddySettings["Nuke"].AsBool()
                            });
                        }
                        AttackBuddySettings["Running"] = true;
                        Chat.WriteLine("Bot enabled.");
                        Start();

                    }
                    else if (AttackBuddySettings["Running"].AsBool() && Toggle && (AttackBuddySettings["Attack"].AsBool() || !AttackBuddySettings["Defend"].AsBool()
                        || AttackBuddySettings["Nuke"].AsBool() || AttackBuddySettings["Roam"].AsBool()))
                    {
                        Stop();
                        Chat.WriteLine("Bot disabled.");
                        IPCChannel.Broadcast(new StopModeMessage()
                        {
                            Attack = AttackBuddySettings["Attack"].AsBool(),
                            Defend = AttackBuddySettings["Defend"].AsBool(),
                            Roam = AttackBuddySettings["Roam"].AsBool(),
                            Nuke = AttackBuddySettings["Nuke"].AsBool()
                        });
                    }
                    return;
                }

                switch (param[0].ToLower())
                {
                    case "ignore":
                        if (param.Length > 1)
                        {
                            string name = string.Join(" ", param.Skip(1));

                            if (!AttackBuddy.NamesToIgnore.Contains(name))
                            {
                                AttackBuddy.NamesToIgnore.Add(name);
                                chatWindow.WriteLine($"Added \"{name}\" to ignored mob list");
                            }
                            else if (AttackBuddy.NamesToIgnore.Contains(name))
                            {
                                AttackBuddy.NamesToIgnore.Remove(name);
                                chatWindow.WriteLine($"Removed \"{name}\" from ignored mob list");
                            }
                        }
                        else
                        {
                            chatWindow.WriteLine("Please specify a name");
                        }
                        break;
                    case "load":
                        if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                            Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                        if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy"))
                            Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy");

                        if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy\\Exports"))
                            Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy\\Exports");

                        string _loadPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\AttackBuddy\Exports\{param[1]}.txt";

                        if (!File.Exists(_loadPath))
                        {
                            Chat.WriteLine($"No such file.");
                            return;
                        }

                        using (StreamReader sr = File.OpenText(_loadPath))
                        {
                            string[] _stringAsArray = sr.ReadLine().Split('-');

                            foreach (string _string in _stringAsArray)
                            {
                                if (_string.Length > 1)
                                {
                                    float x, y, z;

                                    string[] _stringAsArraySplit = _string.Split(',');
                                    if (_string.Contains('.'))
                                    {
                                        x = float.Parse(_stringAsArraySplit[0], CultureInfo.InvariantCulture.NumberFormat);
                                        y = float.Parse(_stringAsArraySplit[1], CultureInfo.InvariantCulture.NumberFormat);
                                        z = float.Parse(_stringAsArraySplit[2], CultureInfo.InvariantCulture.NumberFormat);
                                    }
                                    else
                                    {
                                        x = float.Parse($"{_stringAsArraySplit[0]}.{_stringAsArraySplit[1]}", CultureInfo.InvariantCulture.NumberFormat);
                                        y = float.Parse($"{_stringAsArraySplit[2]}.{_stringAsArraySplit[3]}", CultureInfo.InvariantCulture.NumberFormat);
                                        z = float.Parse($"{_stringAsArraySplit[4]}.{_stringAsArraySplit[5]}", CultureInfo.InvariantCulture.NumberFormat);
                                    }

                                    AttackBuddy.RoamingVectors.Add(new Vector3(x, y, z));
                                }
                            }
                        }

                        break;

                    case "export":
                        if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                            Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

                        if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy"))
                            Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy");

                        if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy\\Exports"))
                            Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy\\Exports");

                        string _exportPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\AOSharp\AttackBuddy\Exports\{param[1]}.txt";

                        if (!File.Exists(_exportPath))
                        {
                            // Create the file.
                            using (FileStream fs = File.Create(_exportPath))
                            {
                                foreach (Vector3 vector in AttackBuddy.RoamingVectors)
                                {
                                    string vectorstring = vector.ToString();
                                    string vectorstring2 = vectorstring.Substring(1, vectorstring.Length - 2);

                                    Byte[] info =
                                            new UTF8Encoding(true).GetBytes($"{vectorstring2}-");

                                    fs.Write(info, 0, info.Length);
                                }
                            }
                        }
                        break;

                    case "addpos":
                        RoamingVectors.Add(DynelManager.LocalPlayer.Position);
                        break;

                    default:
                        return;
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
