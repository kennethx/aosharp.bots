﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class GrabMissionState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Time.NormalTime - InfBuddy._stateTimeOut > 75f)
            {
                if (!InfBuddy._firstTimeout)
                {
                    InfBuddy._firstTimeout = true;
                    InfBuddy._secondTimeout = false;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2717.5f, 24.6f, 3327.4f));
                    return new GrabMissionState();
                }
                else if (!InfBuddy._secondTimeout)
                {
                    InfBuddy._firstTimeout = false;
                    InfBuddy._secondTimeout = true;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2765.7f, 24.6f, 3322.6f));
                    return new GrabMissionState();
                }
            }

            if (Mission.Exists(InfBuddy.Config.CharSettings[Game.ClientInst].GetMissionName()) && !InfBuddy.Config.DoubleReward)
            {
                return new MoveToEntranceState();
            }

            if (InfBuddy.Config.DoubleReward && (Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Me...")) || Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Ha..."))))
            {
                return new MoveToEntranceState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy._stateTimeOut = Time.NormalTime;

            //Chat.WriteLine("GrabMissionState::OnStateEnter");

            //Ignore pets because I already know you people are going to try that.
            //There should be a NPC template associated with this mob that we can use to accurately identify it but i will wait until there's a need.
            Dynel questGiver = DynelManager.Characters.FirstOrDefault(x => x.Name == Constants.QuestGiverName && !x.IsPet);

            if (questGiver == null)
            {
                //Goto MoveToQuestGiver State?
                new MoveToQuestGiverState();
                return;
            }

            NpcDialog.Open(questGiver);
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("GrabMissionState::OnStateExit");
        }

        public void Tick()
        {
            //if (!IsAtQuestGiver())
            //    new MoveToQuestGiverState();
        }
    }
}
