﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfBuddy
{
    public class StartMissionState : IState
    {
        private const string QuestStarterName = "One Who Obeys Precepts";

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.OmniPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.ClanPandeGId)
            {
                InfBuddy.NavMeshMovementController.Halt();
                return new DiedState();
            }

            if (Time.NormalTime - InfBuddy._stateTimeOut > 75f)
            {
                if (!InfBuddy._firstTimeout)
                {
                    InfBuddy._firstTimeout = true;
                    InfBuddy._secondTimeout = false;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2717.5f, 24.6f, 3327.4f));
                    return new StartMissionState();
                }
                else if (!InfBuddy._secondTimeout)
                {
                    InfBuddy._firstTimeout = false;
                    InfBuddy._secondTimeout = true;
                    InfBuddy.NavMeshMovementController.Halt();
                    InfBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(2765.7f, 24.6f, 3322.6f));
                    return new StartMissionState();
                }
            }

            if (!DynelManager.Exists(QuestStarterName))
                return new DefendSpiritState();

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy._stateTimeOut = Time.NormalTime;

            //Chat.WriteLine("StartMissionState::OnStateEnter");

            Dynel questStarter = DynelManager.Characters.FirstOrDefault(x => x.Name == QuestStarterName && !x.IsPet);

            if (questStarter == null)
            {
                //Should we just reform and try again or try to solve this situation?
                Chat.WriteLine("Unable to locate quest starter.");
                return;
            }

            NpcDialog.Open(questStarter);
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("StartMissionState::OnStateExit");
        }

        public void Tick()
        {
        }
    }
}
