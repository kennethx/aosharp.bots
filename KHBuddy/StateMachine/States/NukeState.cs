﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using KHBuddy.IPCMessages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KHBuddy
{
    public class NukeState : IState
    {
        public const double RefreshMongoTime = 8f;
        public const double RefreshAbsorbTime = 11f;

        public double _refreshMongoTimer;
        public double _refreshAbsorbTimer;
        public static float _tetherDistance;

        public SimpleChar LeaderChar;

        Spell aoenukes = null;
        Spell absorb = null;

        public static bool InternelSwitch = false;
        public static bool FirstPass = false;
        public static bool CheckingToMoveSpot = false;

        public IState GetNextState()
        {
            if (Time.NormalTime - KHBuddy._stateTimeOut > 3600f
                && DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
            {
                KHBuddy.KHBuddySettings["Running"] = false;
                Chat.WriteLine("Turning off bot, Idle for too long.");
                return new IdleState();
            }

            if (!KHBuddy.KHBuddySettings["BothSides"].AsBool() && KHBuddy.GameTime > KHBuddy.RespawnTime && InternelSwitch == true &&
                DynelManager.LocalPlayer.Profession != Profession.NanoTechnician)
            {
                InternelSwitch = false;
                return new PullState();
            }

            if (KHBuddy.KHBuddySettings["East"].AsBool() && KHBuddy.KHBuddySettings["BothSides"].AsBool() && InternelSwitch == true &&
                DynelManager.LocalPlayer.Profession != Profession.NanoTechnician)
            {
                if (KHBuddy.GameTime > KHBuddy.RespawnTimeWest && FirstPass == true)
                {
                    KHBuddy.KHBuddySettings["West"] = true;
                    KHBuddy.KHBuddySettings["East"] = false;

                    KHBuddy.IPCChannel.Broadcast(new StartModeMessage()
                    {
                        West = KHBuddy.KHBuddySettings["West"].AsBool(),
                        East = KHBuddy.KHBuddySettings["East"].AsBool(),
                        Beach = KHBuddy.KHBuddySettings["Beach"].AsBool(),
                        BothSides = KHBuddy.KHBuddySettings["BothSides"].AsBool()
                    });

                    InternelSwitch = false;
                    KHBuddy.SwitchingSides = true;
                    return new PullState();
                }
                else if (FirstPass == false)
                {
                    KHBuddy.KHBuddySettings["West"] = true;
                    KHBuddy.KHBuddySettings["East"] = false;

                    KHBuddy.IPCChannel.Broadcast(new StartModeMessage()
                    {
                        West = KHBuddy.KHBuddySettings["West"].AsBool(),
                        East = KHBuddy.KHBuddySettings["East"].AsBool(),
                        Beach = KHBuddy.KHBuddySettings["Beach"].AsBool(),
                        BothSides = KHBuddy.KHBuddySettings["BothSides"].AsBool()
                    });

                    InternelSwitch = false;
                    FirstPass = true;
                    KHBuddy.SwitchingSides = true;
                    return new PullState();
                }    
            }

            if (KHBuddy.KHBuddySettings["West"].AsBool() && KHBuddy.KHBuddySettings["BothSides"].AsBool() && InternelSwitch == true 
                && DynelManager.LocalPlayer.Profession != Profession.NanoTechnician)
            {
                if (KHBuddy.GameTime > KHBuddy.RespawnTimeEast && FirstPass == true)
                {
                    KHBuddy.KHBuddySettings["West"] = false;
                    KHBuddy.KHBuddySettings["East"] = true;

                    KHBuddy.IPCChannel.Broadcast(new StartModeMessage()
                    {
                        West = KHBuddy.KHBuddySettings["West"].AsBool(),
                        East = KHBuddy.KHBuddySettings["East"].AsBool(),
                        Beach = KHBuddy.KHBuddySettings["Beach"].AsBool(),
                        BothSides = KHBuddy.KHBuddySettings["BothSides"].AsBool()
                    });

                    InternelSwitch = false;
                    KHBuddy.SwitchingSides = true;
                    return new PullState();
                }
                else if (FirstPass == false)
                {
                    KHBuddy.KHBuddySettings["West"] = false;
                    KHBuddy.KHBuddySettings["East"] = true;

                    KHBuddy.IPCChannel.Broadcast(new StartModeMessage()
                    {
                        West = KHBuddy.KHBuddySettings["West"].AsBool(),
                        East = KHBuddy.KHBuddySettings["East"].AsBool(),
                        Beach = KHBuddy.KHBuddySettings["Beach"].AsBool(),
                        BothSides = KHBuddy.KHBuddySettings["BothSides"].AsBool()
                    });

                    InternelSwitch = false;
                    FirstPass = true;
                    KHBuddy.SwitchingSides = true;
                    return new PullState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            KHBuddy._stateTimeOut = Time.NormalTime;
            //Chat.WriteLine("NukeState::OnStateEnter");
        }

        public void OnStateExit()
        {
            //Chat.WriteLine("NukeState::OnStateExit");
        }

        private static class RelevantNanos
        {
            public static readonly int[] AOENukes = {28638,
                266297, 28637, 28594, 45922, 45906, 45884, 28635, 28593, 45925, 45940, 45900, 28629,
                45917, 45937, 28599, 45894, 45943, 28633, 28631 };
        }

        public void Tick()
        {
            List<SimpleChar> heck = DynelManager.NPCs
                .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 39f)
                .Where(x => x.IsAlive && x.IsInLineOfSight
                    && !x.IsMoving
                    && x.FightingTarget != null)
                .ToList();
           
            if (DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
            {
                if (aoenukes == null)
                    aoenukes = Spell.List
                        .Where(x => RelevantNanos.AOENukes.Contains(x.Id))
                        .Where(x => x.MeetsSelfUseReqs())
                        .FirstOrDefault();

                if (KHBuddy.KHBuddySettings["East"].AsBool() && KHBuddy.KHBuddySettings["Leash"].AsBool())
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1091.7f, 26.5f, 1051.4f)) > 6f && !MovementController.Instance.IsNavigating)
                    {
                        MovementController.Instance.SetDestination(new Vector3(1091.7f, 26.5f, 1051.4f));
                    }
                }

                if (KHBuddy.KHBuddySettings["West"].AsBool() && KHBuddy.KHBuddySettings["Leash"].AsBool())
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(new Vector3(1064.4f, 25.6f, 1032.6f)) > 6f && !MovementController.Instance.IsNavigating)
                    {
                        MovementController.Instance.SetDestination(new Vector3(1064.4f, 25.6f, 1032.6f));
                    }    
                }

                if (KHBuddy.KHBuddySettings["Beach"].AsBool())
                {
                    List<SimpleChar> hecksatposbeach = DynelManager.NPCs
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 43f)
                            .Where(x => x.IsAlive && x.IsInLineOfSight && x.IsAttacking
                                && !x.IsMoving
                                && x.FightingTarget.Identity != DynelManager.LocalPlayer.Identity
                                && x.Position.DistanceFrom(new Vector3(901.9f, 4.4f, 299.6f)) < 10f)
                            .ToList();

                    if (hecksatposbeach.Count >= 1)
                    {
                        if (!Spell.HasPendingCast && aoenukes.IsReady 
                            && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66))
                        {
                            aoenukes.Cast(hecksatposbeach.FirstOrDefault(), true);
                            KHBuddy._stateTimeOut = Time.NormalTime;
                        }
                    }
                }

                if (KHBuddy.KHBuddySettings["West"].AsBool())
                {
                    List<SimpleChar> hecksatposWest = DynelManager.NPCs
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 60f)
                            .Where(x => x.IsAlive && x.IsInLineOfSight && x.IsAttacking
                                && !x.IsMoving
                                && x.FightingTarget.Identity != DynelManager.LocalPlayer.Identity
                                && x.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1020.5f)) < 10f)
                            .ToList();

                    List<Corpse> hecksatposWestdead = DynelManager.Corpses
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 60f)
                            .Where(x => x.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1020.5f)) < 10f)
                            .ToList();

                    if (hecksatposWest.Count >= 1)
                    {
                        if (!Spell.HasPendingCast && aoenukes.IsReady 
                            && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66))
                        {
                            aoenukes.Cast(hecksatposWest.FirstOrDefault(), true);
                            KHBuddy._stateTimeOut = Time.NormalTime;
                        }
                    }
                }

                if (KHBuddy.KHBuddySettings["East"].AsBool())
                {
                    List<SimpleChar> hecksatposEast = DynelManager.NPCs
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 60f)
                            .Where(x => x.IsAlive && x.IsInLineOfSight && x.IsAttacking
                                && !x.IsMoving
                                && x.FightingTarget.Identity != DynelManager.LocalPlayer.Identity
                                && x.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 10f)
                            .ToList();

                    List<Corpse> hecksatposEastdead = DynelManager.Corpses
                            .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                            .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 60f)
                            .Where(x => x.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 10f)
                            .ToList();

                    if (hecksatposEast.Count >= 1)
                    {
                        if (!Spell.HasPendingCast && aoenukes.IsReady 
                            && (DynelManager.LocalPlayer.NanoPercent >= 66 || DynelManager.LocalPlayer.HealthPercent >= 66))
                        {
                            aoenukes.Cast(hecksatposEast.FirstOrDefault(), true);
                            KHBuddy._stateTimeOut = Time.NormalTime;
                        }
                    }
                }
            }

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                List<SimpleChar> hecksatposEastdead = DynelManager.NPCs
                    .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !x.IsAlive && x.IsInLineOfSight
                        && x.Position.DistanceFrom(new Vector3(1115.9f, 1.6f, 1064.3f)) < 8f)
                    .ToList();

                List<SimpleChar> hecksatposWestdead = DynelManager.NPCs
                    .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !x.IsAlive && x.IsInLineOfSight
                        && x.Position.DistanceFrom(new Vector3(1043.2f, 1.6f, 1020.5f)) < 8f)
                    .ToList();

                List<SimpleChar> hecksatposbeachdead = DynelManager.NPCs
                    .Where(x => x.Name.Contains("Heckler") || x.Name.Contains("Voracious"))
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !x.IsAlive && x.IsInLineOfSight
                        && x.Position.DistanceFrom(new Vector3(901.9f, 4.4f, 299.6f)) < 8f)
                    .ToList();

                Spell.Find(270786, out Spell mongobuff);

                if (KHBuddy.KHBuddySettings["Beach"].AsBool() && InternelSwitch == false && heck.Count == 0)
                {
                    if (!KHBuddy.KHBuddySettings["BothSides"].AsBool() && hecksatposbeachdead.Count >= 1)
                    {
                        KHBuddy.RespawnTime = KHBuddy.GameTime.AddSeconds(720);
                        InternelSwitch = true;
                    }
                }

                if (KHBuddy.KHBuddySettings["East"].AsBool() && InternelSwitch == false && heck.Count == 0)
                {
                    if (!KHBuddy.KHBuddySettings["BothSides"].AsBool() && hecksatposEastdead.Count >= 1)
                    {
                        KHBuddy.RespawnTime = KHBuddy.GameTime.AddSeconds(720);
                        InternelSwitch = true;
                    }

                    if (KHBuddy.KHBuddySettings["BothSides"].AsBool() && hecksatposEastdead.Count >= 1)
                    {
                        KHBuddy.RespawnTimeEast = KHBuddy.GameTime.AddSeconds(720);
                        InternelSwitch = true;
                    }
                }

                if (KHBuddy.KHBuddySettings["West"].AsBool() && InternelSwitch == false && heck.Count == 0)
                {
                    if (!KHBuddy.KHBuddySettings["BothSides"].AsBool() && hecksatposWestdead.Count >= 1)
                    {
                        KHBuddy.RespawnTime = KHBuddy.GameTime.AddSeconds(720);
                        InternelSwitch = true;
                    }

                    if (KHBuddy.KHBuddySettings["BothSides"].AsBool() && hecksatposWestdead.Count >= 1)
                    {
                        KHBuddy.RespawnTimeWest = KHBuddy.GameTime.AddSeconds(720);
                        InternelSwitch = true;
                    }
                }

                if (absorb == null)
                    absorb = Spell.List.Where(x => x.Nanoline == NanoLine.AbsorbACBuff).OrderBy(x => x.StackingOrder).FirstOrDefault();

                if (heck.Count >= 1)
                {
                    if (!Spell.HasPendingCast && mongobuff.IsReady && Time.NormalTime > _refreshMongoTimer + RefreshMongoTime)
                    {
                        mongobuff.Cast();
                        _refreshMongoTimer = Time.NormalTime;
                    }
                    if (!Spell.HasPendingCast && absorb.IsReady && Time.NormalTime > _refreshAbsorbTimer + RefreshAbsorbTime)
                    {
                        absorb.Cast();
                        _refreshAbsorbTimer = Time.NormalTime;
                    }
                }
            }
        }
    }
}
