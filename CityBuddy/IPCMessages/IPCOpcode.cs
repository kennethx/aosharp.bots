﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 1101,
        Stop = 1102
    }
}
